package controller.lateralcontroller;

import controller.MainController;
import model.MainModel;
import network.packet.Channel;
import view.MainView;
import view.left.MenuBarView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Controlador del mous el la parte lateral izquierda
 *
 * @author : David
 */
public class LateralMouseController extends MouseAdapter {

    private MainView view;
    private MainModel model;
    private Component auxComponent;
    private MainController mainController;
    private LateralDocumentController docController;

    private static final Color SELECTED_CHANNEL = new Color(140,110,140);
    private static final Color UNSELECTED_CHANNEL = new Color(100,70,100);

    /**
     * Constructor
     * @param view vista principal
     * @param model modelo
     * @param controller controlador
     * @param docController doc controller
     */
    public LateralMouseController(MainView view, MainModel model, MainController controller,
                                  LateralDocumentController docController){
        this.view = view;
        this.model = model;
        auxComponent = null;
        this.mainController = controller;
        this.docController = docController;
    }

    /**
     * Cuando se presiona sobre un canal, chat o en eliminar canal privado
     * @param e evento
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getComponent().getName().charAt(1) == 'X') {
            /* Boton de borrar canal */
            if(Integer.valueOf(e.getComponent().getName().substring(3)) == model.getCurrentChannel().getId()){
                view.mostraWelcome();
                view.setInfoCanalVisible(false);
            }
            view.deleteChannel(Integer.parseInt(e.getComponent().getName().substring(3)));
        } else if(e.getComponent().getName().equals(MenuBarView.PROFILE)){
            /* Ventana de perfil */
            view.getJpBarraLateral().unselectChannel();
            if (auxComponent != null) {
                auxComponent.setBackground(UNSELECTED_CHANNEL);
                ((JLabel) auxComponent).setOpaque(true);
            }
            model.setCurrentChannel(null);
            view.setInfoCanalVisible(false);
            view.mostraUserInfo(model.getProfile().getUsername(),model.getProfile().getEmail(),
                    model.getProfile().getStatus(),model.getProfile().getProfileImage());
        }else {
            if (e.getComponent().getName().equals(MenuBarView.DELETE)) {
                docController.deleteInputText();
            } else {
                /* Se ha pulsado un canal */
                String channel = e.getComponent().getName();
                if(channel.length() > 3) {
                    Integer idChannel = Integer.valueOf(e.getComponent().getName().substring(3));
                    /* No cargar todo si es el mismo canal */
                    if (model.getCurrentChannel() == null || !(model.getCurrentChannel().getId() == idChannel)) {
                        view.setInfoCanalVisible(false);
                        mainController.channelChanged();
                        view.getJpBarraLateral().unselectChannel();
                        if (auxComponent != null) {
                            auxComponent.setBackground(UNSELECTED_CHANNEL);
                            ((JLabel) auxComponent).setOpaque(true);
                        }
                        JLabel jLabel = (JLabel) e.getComponent();
                        jLabel.setBackground(SELECTED_CHANNEL);
                        jLabel.setOpaque(true);
                        auxComponent = e.getComponent();
                        Channel channelToShow;
                        view.clearChatView();
                        if (channel.charAt(1) == '#') {
                            /* Canal publico */
                            channelToShow = model.changeChannel(false, idChannel);
                        } else {
                            /* Canal privado */
                            channelToShow = model.changeChannel(true, idChannel);
                        }
                        if (channelToShow.isPrivate()) {
                            /* Canal privado */
                            view.loadChannelInfo(model.getPrivateChannelName(channelToShow), channelToShow.getDescription(), channelToShow.getUsers());
                        } else {
                            /* Canal publico */
                            view.loadChannelInfo(channelToShow.getName(), channelToShow.getDescription(), channelToShow.getUsers());
                        }
                        if (channelToShow.getMessages() == null) {
                            /* Si no tiene mensajes cargados hacemos la peticion */
                            channelToShow.setMessages(new ArrayList<>());
                            view.mostraLoading();
                            model.askForMessages(channelToShow.getName(), channelToShow.getId(), 0);
                        } else {
                            /* Si tiene mensajes cargados los cargamos */
                            view.mostraContentView();
                            mainController.loadChannelMessages(channelToShow.getMessages(), channelToShow.getLastViewedMessageId());
                        }
                    }
                }
            }
        }
    }

    /**
     * Metodo para deseleccionar el ultimo canal seleccionado
     */
    public void unselectedChannel(){
        if(auxComponent != null) {
            auxComponent.setBackground(UNSELECTED_CHANNEL);
        }
    }
}
