package controller.lateralcontroller;

import main.Main;
import model.MainModel;
import network.ServerCommunication;
import network.packet.Profile;
import network.packet.UserStatusChanged;
import view.MainView;
import view.left.MenuBarView;
import view.center.UserView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controlador de la vista de usuario
 *
 * @author : Daniel
 */
public class UserViewController implements ActionListener {

    private UserView view;
    private MenuBarView menuBarView;
    private ServerCommunication comm;
    private MainModel model;
    private MainView mainView;

    /**
     * Verifica que accion tomar
     * Cambiar el estado del usuario
     * Desconectar al usaurio
     * @param e Evento
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case UserView.CONNECTED:
            case UserView.OCCUPIED:
                /* Si se cambia el estado actualizamos la vista */
                menuBarView.setEstado(e.getActionCommand());
                model.setStatus(e.getActionCommand());
                view.setStatus(e.getActionCommand());
                comm.sendPacket(new UserStatusChanged(model.getProfile().getEmail(), e.getActionCommand()));
                break;
            case UserView.LOGOUT:
                comm.sendPacket(new UserStatusChanged(model.getProfile().getEmail(), Profile.DISCONNECTED));
                comm.stopConnection();
                mainView.dispose();
                Main.main(null);
                break;
        }
    }

    /**
     * HAce un set a las vistas
     * @param view user view
     * @param menuBarView menu bar view
     */
    public void setViews(UserView view, MenuBarView menuBarView) {
        this.view = view;
        this.menuBarView = menuBarView;
    }

    /**
     * Set del modelo
     * @param model modelo
     */
    public void setModel(MainModel model){
        this.model = model;
    }

    /**
     * Set del network (server communication)
     * @param comm server com
     */
    public void setNetwork(ServerCommunication comm){
        this.comm = comm;
    }

    /**
     * Set main view
     * @param mainView main view
     */
    public void setMainView(MainView mainView){
        this.mainView = mainView;
    }
}
