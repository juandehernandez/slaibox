package controller.lateralcontroller;

import model.MainModel;
import view.left.MenuBarView;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Controller encargado de implementar el document listener del buscador
 *
 * @author : David
 */
public class LateralDocumentController implements DocumentListener{

    private boolean changed;
    private MenuBarView menuBarView;
    private MainModel model;

    /**
     * Constructor
     * @param menuBarView vista del menu
     * @param model modelo
     */
    public LateralDocumentController(MenuBarView menuBarView,MainModel model){
        changed = false;
        this.menuBarView = menuBarView;
        this.model = model;
    }

    /**
     * Cuando se escribe algo en el buscador
     * @param e evento
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        changed = true;
        try {
            searchChannels(e.getDocument().getText(0,e.getDocument().getLength()));
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Cuando se borra algo del buscador
     * @param e evento
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        String text = null;
        try {
            /* No se puede hacer un getText solo asi k utilizamos offset 0 */
            text = e.getDocument().getText(0,e.getDocument().getLength());
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }
        if(text != null && text.equals("")) {
            changed = false;
        }
        if (changed) {
            searchChannels(text);
        }else{
            menuBarView.mostraTotsCanals();
        }
    }

    /**
     * Cuando se actualiza algo en la barra de busqueda
     * @param e evento
     */
    @Override
    public void changedUpdate(DocumentEvent e) {
        if(changed) {
            try {
                searchChannels(e.getDocument().getText(0,e.getDocument().getLength()));
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }else{
            menuBarView.mostraTotsCanals();
        }
    }

    /**
     * Metodo que inicia la busqueda de canales similares a partir del texto
     * @param contenido texto a comparar
     */
    private void searchChannels(String contenido){
        model.getCanalesNoSimilares(contenido);
    }


    /**
     * Borra todo el texto del buscador y vuelve a mostrar todos los canales
     */
    public void deleteInputText(){
        menuBarView.deleteText();
        menuBarView.mostraTotsCanals();
    }

    /**
     * Metodo que se encarga de incrementar el valor de mensajes no leidos
     * @param channel canal al que incrementar
     */
    public void incrementaUnseen(int channel){
        menuBarView.incrementaUnseen(channel);
    }

    /**
     * Metodo que establece el numero de mensajes no leidos
     * @param channel canal al que establecer
     * @param number numero de mensajes sin leer
     */
    public void setUnseenMessage(int channel, int number){
        menuBarView.setUnseenMessage(channel, number);
    }

    /**
     * Establece los mensajes como leidos
     * @param channel canal a establecer
     */
    public void setSeenMessages(int channel){
        menuBarView.setSeenMessages(channel);
    }
}
