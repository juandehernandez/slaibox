package controller.credentials;

import network.ServerCommunication;
import view.credentials.RegisterView;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

/**
 * Controlador de la vista de registro
 *
 * @author Daniel
 * @version 1.0.0
 */
public class RegisterController extends MouseAdapter {

    private RegisterView view;
    private ServerCommunication comm;

    /**
     * Constructor
     * @param view vista del registro
     * @param comm comunicacion con el servidor
     */
    public RegisterController(RegisterView view, ServerCommunication comm) {
        this.view = view;
        this.comm = comm;
    }

    /**
     * Cuando se presiona un boton, aqui se gestiona
     *
     * @param e evento
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        String user = view.getUsername();
        String email = view.getEmail();
        char[] pwd = view.getPassword();
        char[] pwdVerify = view.getConfirmedPassword();
        if (Arrays.equals(pwd, pwdVerify) && pwd.length > 0) {
            if (!user.isEmpty()) {
                if (!(email.isEmpty() || !email.contains("@") || !email.contains("."))) {
                    comm.sendRegister(email, user, pwd, pwdVerify);
                } else {
                    messageBox("Email doesn't seem to be valid", "Invalid email", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                messageBox("Username can't be empty!", "Invalid username", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            messageBox("Passwords must match!", "Password mismatch", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Metodo utilizado para mostrar un mensaje popup
     *
     * @param body  contenido del mensaje
     * @param title titulo del mensaje
     * @param type  tipo del mensaje (error, informacion, advertencia, etc)
     */
    public void messageBox(String body, String title, int type) {
        SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(view, body, title, type);
        });
    }

    /**
     * Cierra la ventana de registro
     */
    public void closeRegister() {
        view.dispose();
    }
}
