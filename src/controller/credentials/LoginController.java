package controller.credentials;

import network.ServerCommunication;
import view.credentials.LoginView;
import view.credentials.RegisterView;

import javax.swing.*;
import java.awt.event.*;

/**
 * Controlador de la vista de login
 *
 * @author Daniel
 * @version 1.0.0
 */
public class LoginController extends MouseAdapter implements KeyListener {

    private LoginView view;
    private RegisterView registerView;
    private ServerCommunication serverCommunication;

    /**
     * Constructor
     * @param view vista del login
     * @param registerView vista del registro
     * @param serverCommunication comunicacion con el servidor
     */
    public LoginController(LoginView view, RegisterView registerView, ServerCommunication serverCommunication) {
        this.view = view;
        this.registerView = registerView;
        this.serverCommunication = serverCommunication;
    }

    /**
     * Cuando se presiona un boton, aqui se gestiona
     *
     * @param e evento
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        JButton src = (JButton) e.getSource();
        if (src.isEnabled()) {
            if (!src.getText().equals("Register")) {
                String user = view.getUser();
                char[] pwd = view.getPassword();
                if (user.isEmpty() || pwd.length == 0) {
                    JOptionPane.showMessageDialog(
                            view, "All fields must be filled!", "Invalid data", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    //Login
                    serverCommunication.sendLogin(view.getUser(), view.getPassword());
                }
            } else {
                registerView.setVisible(true);
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Usado para cuando se presiona ENTER y comenzar el inicio de sesion
     *
     * @param e evento
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            view.pressLogin();
            //generar nuevo evento
            mouseReleased(new MouseEvent(view.getBtnLogin(), MouseEvent.MOUSE_RELEASED,
                    System.currentTimeMillis(), InputEvent.META_MASK, 0, 0, 1, false,
                    1));
        }
    }

    /**
     * Cierra la ventana de login
     */
    public void closeLogin(){
        view.dispose();
    }

    /**
     * Desactiva el boton de login
     */
    public void unableLogin(){view.unableLoginButton();}

    /**
     * Activa el boton de login
     */
    public void enableLogin(){view.enableLoginButton();}
}
