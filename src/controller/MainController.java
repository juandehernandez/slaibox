package controller;

import controller.credentials.LoginController;
import controller.credentials.RegisterController;
import controller.cvcontroller.CVButtonController;
import controller.lateralcontroller.LateralDocumentController;
import controller.lateralcontroller.LateralMouseController;
import model.MainModel;
import network.ServerCommunication;
import network.packet.*;
import util.CurrentDate;
import util.FileUtils;
import view.MainView;
import view.left.MenuBarView;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static network.packet.Message.EMPTY_MESSAGE;

/**
 * Controlador general del proyecto
 *
 * @author : David
 */
public class MainController extends WindowAdapter {

    private MainView view;
    private LoginController loginController;
    private RegisterController registerController;
    private ServerCommunication comm;
    private MainModel model;
    private boolean loadingMessages = false;
    private CVButtonController cvButtonController;
    private LateralDocumentController lateralController;
    private LateralMouseController lateralMouseController;

    /**
     * Constructor
     * @param view vista principal
     * @param loginController controlador de la vista del login
     * @param registerController controlador de la vista del registro
     * @param comm comunicacion con el servidor
     */
    public MainController(MainView view, LoginController loginController,
                          RegisterController registerController, ServerCommunication comm) {
        this.view = view;
        this.loginController = loginController;
        this.registerController = registerController;
        this.comm = comm;
    }

    /**
     * Setter del model
     * @param model model
     */
    public void setModel(MainModel model) {
        this.model = model;
    }

    /**
     * Metodo que carga toda la informacion en la vista
     * @param profile Perfil de nuestro usuario
     * @param channelsName Lista con todos los canales
     * @param chatsName Lista con todos los chats (privados)
     * @param publicIds Ides de los canales publicos
     * @param privateIds Ides de los canales privados
     */
    public void showAllInfo(Profile profile, List<String> channelsName, List<String> chatsName,
                            List<Integer> publicIds,List<Integer> privateIds){
        MenuBarView barra = view.getJpBarraLateral();
        barra.addCanals(channelsName,publicIds);
        barra.addChats(chatsName,privateIds);
        barra.setProfile(profile.getUsername(),profile.getStatus(),profile.getProfileImage());
    }

    /**
     * Devuelve la main view
     * @return main view
     */
    public MainView getView() {
        return view;
    }

    /**
     *  Devuelve el controlador del login
     * @return controlador del login
     */
    public LoginController getLoginController() {
        return loginController;
    }

    /**
     * Registra el controlador de registro
     * @return controlador
     */
    public RegisterController getRegisterController() {
        return registerController;
    }

    /**
     * Devuelve el server communication
     * @return server communication
     */
    public ServerCommunication getComm() {
        return comm;
    }

    /**
     * Muesta la vista inicial del programa
     */
    public void showMainView(){
        SwingUtilities.invokeLater(() -> {
            view.mostraWelcome();
            view.setVisible(true);
        });
    }

    /**
     * Establece los controladores de la barra lateral izquierda
     * @param controller Document controller
     * @param lateralMouseController Mouse Controller
     */
    public void setLateralController(LateralDocumentController controller,LateralMouseController lateralMouseController) {
        lateralController = controller;
        this.lateralMouseController = lateralMouseController;
    }

    /**
     * Carga los mensajes del canal correspondiente
     * @param messages Lista de mensajes
     * @param lastMessageIdViewed Id del ultimo mensaje visto
     *
     */
    public void loadChannelMessages(ArrayList<Message> messages, int lastMessageIdViewed) {
        Message lastMessage = null;
        //view.mostraContentView();
        view.clearChatView();
		
        int size = messages.size();
        boolean notViewedNotShowed = false;

        //mete el boton de cargar mas mensajes
        view.addRequestMoreMessages();
        //verifica si el boton tiene que estar deshabilitado
        if( messages.size() >= model.getCurrentChannel().getTotalMessagesNumber()){
            view.setEnableRequestMoreMessages(false);
        }
        view.prepareChatView();
        loadingMessages = true;
        for (int i = 0; i < messages.size() ; i++){
            Message msg = messages.get(i);
            Profile profile = model.getProfileFromEmail(msg.getEmailUser());
            if (msg instanceof Text){
                //añadir mensajes de texto
                loadChannelText(lastMessage, (Text) msg, profile.getProfileImage(), lastMessageIdViewed);
            }else {
                //añadir mensajes de archivos
                loadChannelFile(lastMessage,(network.packet.File) msg, profile.getProfileImage(), lastMessageIdViewed);
            }
            lastMessage = msg;
        }
        loadingMessages = false;
        view.mostraContentView();
        view.scrollToBottom();
    }

    /**
     * Carga los mensajes en el canal
     * @param lastMessage ultimo mensaje enviado
     * @param t archivo de texto a mostrar
     */
    public void loadChannelText(Message lastMessage, Text t, ImageIcon image, int lastId){
        String user = t.getEmailUser();
        String myUser = model.getProfile().getEmail();
        boolean addedPane = false;
        boolean isScrollOnBottom = view.isScrollOnBottom();
        if (loadingMessages) {
            if (t.getMessageId() > lastId && !model.getProfile().getEmail().equals(t.getEmailUser())) {
                addedPane = view.incrementNotViewedNotification();
            }
        }

        if (!isScrollOnBottom &&  !loadingMessages && !user.equals(myUser)) {
            addedPane = view.incrementNotViewedNotification();
        }
        if (Message.isNewMessage(lastMessage,t) || addedPane){
            if (lastMessage != null && CurrentDate.nextDay(lastMessage.getDate(),t.getDate())){
                view.addNewDayNotification(new CurrentDate(t.getDate().getTime()).infoToString());
            }
            view.addFirstMessage(t.getMessage(),user,image,t.getDate(),user.equals(myUser));
        }else{
            view.addMessage(t.getMessage(),t.getDate(), user.equals(myUser));
        }
        if (isScrollOnBottom || user.equals(myUser)) {
            view.scrollToBottom();
        }
    }

    /**
     * Carga los archivos en el canal
     * @param lastMessage ultimo mensaje enviado
     * @param file archivo a cargar
     * @param image imagen del usuario que envia el mensaje
     */
    public void loadChannelFile(Message lastMessage, network.packet.File file, ImageIcon image, int lastId){
        String user = file.getEmailUser();
        String myUser = model.getProfile().getEmail();
        boolean addedPane = false;
        boolean isScrollOnBottom = view.isScrollOnBottom();;
        if (loadingMessages) {
            if (file.getMessageId() > lastId && !model.getProfile().getEmail().equals(file.getEmailUser())) {
                addedPane = view.incrementNotViewedNotification();
            }
        }

        if (!isScrollOnBottom &&  !loadingMessages && !user.equals(myUser)) {
            addedPane = view.incrementNotViewedNotification();
        }
        if (Message.isNewMessage(lastMessage,file) || addedPane){
            if (lastMessage != null && CurrentDate.nextDay(lastMessage.getDate(),file.getDate())){
                view.addNewDayNotification(new CurrentDate(file.getDate().getTime()).infoToString());
            }
            view.addFirstMessage(EMPTY_MESSAGE,user,image,file.getDate(),user.equals(model.getProfile().getEmail()));
            loadFile(file);
        }else{
            loadFile(file);
        }
        if (isScrollOnBottom || user.equals(myUser)) {
            view.scrollToBottom();
        }
    }

    /**
     * Carga especificamente una imagen/archivo en canal
     * @param file archivo a cargar
     */
    private void loadFile(network.packet.File file){
        CurrentDate cDate = new CurrentDate(file.getDate().getTime());
        if (FileUtils.isPicture(file.getName())){
            view.addImageToMessage(cDate,FileUtils.getImageBufferedImage(file), file.getMessageId());
        }else{
            view.addFileToMessage(cDate,FileUtils.getFileBufferedImage(file),file.getName(), file.getMessageId());
        }
    }

    /**
     * Activa o desactiva el boton de cargar mas mensajes
     * @param bool
     */
    public void setEnableRequestMoreMessages(boolean bool){ view.setEnableRequestMoreMessages(bool); }

    /**
     * Metodo que cierra la vista de registro
     */
    public void closeRegister(){
        registerController.closeRegister();
    }

    /**
     * Metodo que cierra la ventana de login
     */
    public void closeLogin(){
        loginController.closeLogin();
    }

    /**
     * Desactiva las opciones de login
     */
    public void unableLogin(){
        loginController.unableLogin();
        if(view != null){
            view.dispose();
        }
    }

    /**
     * Activa las opciones de login
     */
    public void enableLogin(){
        loginController.enableLogin();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        comm.stopConnection();
    }

    /**
     * Metodo llamado cuando la busqueda de canales a esconder acaba,
     * llama a la vista para indicar que canales debe esconder
     *
     * @param aList Lista con los id de los canales a esconder
     */
    public void searchEnded(ArrayList<Integer> aList){
        if(aList.size() > 0) {
            view.getJpBarraLateral().escondeCanales(aList);
        }
    }

    /**
     * Muestra un mensaje de texto
     *
     * @param msg texto del mensaje a mostrar
     */
    public void showMessage(String msg, int type) {
        JOptionPane.showMessageDialog(view, msg, "", type);
    }

    /**
     * Elimina el canal de la vista
     * @param channel nombre del canal
     * @param isVisible indica si el canal se esta mostrando actualmente por la vista o no
     */
    public void deleteChannelFromView(int channel, boolean isVisible){
        view.deleteChannel(channel);
        view.setInfoCanalVisible(false);
        if(isVisible) view.mostraWelcome();
    }

    /**
     * Incrementa el numero de mensajes no leidos
     * @param channel
     */
    public void incrementChannel(int channel){
        view.getJpBarraLateral().incrementaUnseen(channel);
    }

    /**
     * Metodo que actualiza la vista incrementando el numero de usuarios del canal
     * i actualiza la barra de informacion del canal
     */
    public void incrementChannelUsers(){
        Channel channel = model.getCurrentChannel();
        view.updateChannelInfo(channel.getUsers());
        cvButtonController.infoChanged();
    }

    /**
     * Actualiza la barra de informacion del canal
     */
    public void channelChanged(){
        cvButtonController.channelChanged();
    }

    /**
     * Establece el controllador de la Content view
     * @param cvButtonController Button controller
     */
    public void setCvButtonController(CVButtonController cvButtonController){
        this.cvButtonController = cvButtonController;
    }

    /**
     * Incrementa el numero de mensajes sin leer
     * @param channel id del canal a incrementar
     */
    public void incrementaUnseen(int channel) {
        lateralController.incrementaUnseen(channel);
    }

    /**
     * Establece el numero de mensajes sin leer
     * @param channel id del canal a incrementar
     * @param number numero a establecer
     */
    public void setUnseenMessage(int channel, int number) {
        lateralController.setUnseenMessage(channel, number);
    }

    /**
     * Establece que en el canal escogido se han leido todos los mensajes
     * @param channel canal
     */
    public void setSeenMessages(int channel) {
        lateralController.setSeenMessages(channel);
    }

    /**
     * Añade un canal a la lista lateral de canales
     * @param name nombre del canal
     * @param privateCh booleano que indica su privacidad
     * @param id identificador
     */
    public void addChannelToList(String name, boolean privateCh, int id){
        view.getJpBarraLateral().addChannel(name,privateCh,id);
    }

    /**
     * Avisa de actualizar la informacion del canal
     */
    public void infoChannelChanged(){
        cvButtonController.infoChanged();
    }

    /**
     * Avisa de que se ha cambiado un estatus
     * @param idChannel identificador del canal
     * @param status estado del usuario
     */
    public void changeConnection(int idChannel, String status){
        view.getJpBarraLateral().changeConnection(idChannel,status);
    }

    /**
     * Deselecciona el ultimo canal seleccionado
     */
    public void unselectLastChannel(){
        lateralMouseController.unselectedChannel();
    }

    /**
     * carga un canal privado
     * @param channel nombre del canal
     * @param idChannel id del canal
     */
    public void loadPrivate(String channel,int idChannel){
        unselectLastChannel();
        view.getJpBarraLateral().setSelectedChannel(idChannel);
        //canal privado ya existente
        Channel channelToShow = model.changeChannel(true, idChannel);
        view.loadChannelInfo(channel, channelToShow.getDescription(), channelToShow.getUsers());
        infoChannelChanged();
        if (channelToShow.getMessages() == null) {
            view.mostraLoading();
            model.askForMessages(channelToShow.getName(), channelToShow.getId(), 0);
        } else {
            view.mostraContentView();
            loadChannelMessages(channelToShow.getMessages(), channelToShow.getLastViewedMessageId());
        }
    }
}
