package controller.cvcontroller;

import model.MainModel;
import network.ServerCommunication;
import view.MainView;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Controlador de teclas y focus del chat
 *
 * @author Matias
 * @version 1.0.0
 */
public class CVKeyListener extends KeyAdapter implements FocusListener{

    private MainView view;
    private MainModel model;
    private ServerCommunication serverCom;

    /**
     * Constructor de la clase CVKeyListener
     * @param view vista principal
     * @param model modelo principal
     * @param serverCom gestor de la comunicacion
     */
    public CVKeyListener(MainView view, MainModel model, ServerCommunication serverCom){
        this.view = view;
        this.model = model;
        this.serverCom = serverCom;
    }

    /**
     * Gestiona cuando se presiona enter
     *
     * @param e evento
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            view.getContentView().getBottomBarView().sendClick();
        }
    }

    /**
     * Baja al fondo cuando gana focus el chat
     *
     * @param e evento
     */
    @Override
    public void focusGained(FocusEvent e) {
        view.scrollToBottom();
        view.clearNotViewedNotification();
        model.updateLastViewedMessage();
    }

    @Override
    public void focusLost(FocusEvent e) {

    }
}
