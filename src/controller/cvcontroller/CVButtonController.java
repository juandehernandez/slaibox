package controller.cvcontroller;

import controller.MainController;
import model.MainModel;
import network.ServerCommunication;
import network.packet.Channel;
import network.packet.request.CreatePrivateChannelRequest;
import view.MainView;
import view.right.InfoCanal;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;

/**
 * Controlador de los botones del chat
 *
 * @author Matias
 * @version 3.0.0
 */
public class CVButtonController implements ActionListener {

    private ServerCommunication serverCom;
    private final JFileChooser fc;
    private InfoCanal infoCanal;
    private MainView mainView;
    private MainModel mainModel;
    private SimpleDateFormat formatterInfo;
    private MainController controller;
    private boolean primerCop;

    /**
     * Constructor de la clase
     *
     * @param mainView  instancia de la vista principal
     * @param serverCom instancia de la comunicacion con el servidor
     * @param mainModel instancia del modelo principal
     */
    public CVButtonController(MainView mainView, ServerCommunication serverCom, MainModel mainModel) {
        this.mainView = mainView;
        this.serverCom = serverCom;
        this.mainModel = mainModel;
        formatterInfo = new SimpleDateFormat("dd-MM-yyyy");
        infoCanal = mainView.getInfoCanal();
        fc = new JFileChooser();
        primerCop = true;
    }

    /**
     * Controlador del Chat View
     *
     * @param e evento generado
     */
    @Override
    public void actionPerformed(ActionEvent e) {


        if (e.getActionCommand().equals("SEND_MESSAGE")) {

            String message = mainView.getMessage();

            /* verificamos que hay texto */
            if (message != null && !message.isEmpty()) {
                String user_email = mainModel.getProfile().getEmail();
                int id_canal = mainModel.getCurrentChannel().getId();
                serverCom.sendText(message, user_email, id_canal);
                mainView.eraseMessage();
            }

        } else if(e.getActionCommand().equals("TEXT_FIELD")){


        } else if (e.getActionCommand().equals("ADD_ATTACHMENT")) {
            int returnVal = fc.showOpenDialog(mainView);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                try {
                    File file = fc.getSelectedFile();

                    byte[] data = new byte[(int) file.length()];
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    bis.read(data, 0, data.length);

                    String user_email = mainModel.getProfile().getEmail();
                    int idChannel = mainModel.getCurrentChannel().getId();

                    serverCom.sendFile(user_email, data, file.getName(), idChannel);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        } else if (e.getActionCommand().equals("FILES")) {
            if (infoCanal.getSelectedTab() == 2) {
                mainView.setInfoCanalVisible(false);
            } else {
                if(primerCop) {
                    mainView.removeInfoCanal();
                }
                loadRightInfo();
                mainView.setInfoCanalVisible(true);
                infoCanal.setFilesInfo();
            }

        } else if (e.getActionCommand().equals("LOAD_MORE_MESSAGES")) {
            Channel cc = mainModel.getCurrentChannel();
            mainView.mostraLoading();
            //actualizar el offset
            cc.incrementOffset();
            serverCom.askForMessages(cc.getName(), cc.getId(), cc.getMessageOffset());

        }else if(e.getActionCommand().equals("nuevo_chat")) {
            JButton jb = (JButton) e.getSource();
            int idChannel = mainModel.existsPrivateChannel(jb.getName());
            if(idChannel != -1) {
                mainView.setInfoCanalVisible(false);
                controller.unselectLastChannel();
                mainView.getJpBarraLateral().setSelectedChannel(idChannel);
                //canal privado ya existente
                Channel channelToShow = mainModel.changeChannel(true, idChannel);
                if (channelToShow.isPrivate()) {
                    mainView.loadChannelInfo(mainModel.getPrivateChannelName(channelToShow), channelToShow.getDescription(), channelToShow.getUsers());

                } else {
                    mainView.loadChannelInfo(channelToShow.getName(), channelToShow.getDescription(), channelToShow.getUsers());

                }
                if (channelToShow.getMessages() == null) {
                    mainView.mostraLoading();
                    mainModel.askForMessages(channelToShow.getName(), channelToShow.getId(), 0);
                } else {
                    mainView.mostraContentView();
                    controller.loadChannelMessages(channelToShow.getMessages(), channelToShow.getLastViewedMessageId());
                }
            }else{
                String[] options = {"SI", "NO"};
                int action = JOptionPane.showOptionDialog(null,
                        "¿Quieres abrir un chat privado? ",
                        "Nuevo chat privado",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,options, options[1]);
                if(action == JOptionPane.OK_OPTION) {
                    //creamos un nuevo canal privado y lo enviamos por serverCom
                    CreatePrivateChannelRequest chatPrivado = new CreatePrivateChannelRequest(jb.getName(),
                            mainModel.getProfile().getEmail());
                    serverCom.sendPacket(chatPrivado);
                }
            }


        } else {
            if (e.getActionCommand().equals("CHANNEL_DETAILS")) {
                if (infoCanal.getSelectedTab() == 0) {
                    mainView.setInfoCanalVisible(false);
                } else {
                    if(primerCop) {
                        mainView.removeInfoCanal();
                    }
                    loadRightInfo();
                    mainView.setInfoCanalVisible(true);
                    infoCanal.setChannelInfo();
                }
            }

        }

    }

    /**
     * Metodo para cargar la info en la barra derecha
     */
    private void loadRightInfo(){
        if(primerCop){
            if(mainModel.getCurrentChannel() != null) {
                if (mainModel.getCurrentChannel().isPrivate()) {
                    infoCanal.setNameChannel(mainModel.getPrivateChannelName(mainModel.getCurrentChannel()));
                } else {
                    infoCanal.setNameChannel(mainModel.getCurrentChannel().getName());
                }
                infoCanal.setDescriptionChannel(mainModel.getCurrentChannel().getDescription());
                infoCanal.setFechaChannel(formatterInfo.format(mainModel.getCurrentChannel().getDate()));
                infoCanal.setNumMissatges(mainModel.getCurrentChannel().getTotalMessagesNumber());
                infoCanal.setUsuariInfo(mainModel.getProfiles(mainModel.getCurrentChannel())
                        ,mainModel.getProfile().getEmail());
                infoCanal.setFilesInfoView(mainModel.getCurrentChannel().getFilesInfo());
                primerCop = false;
            }
        }
    }

    /**
     * Metodo para resetear la variable de primera vez
     */
    public void channelChanged(){
        primerCop = true;
    }

    /**
     * Actualiza la barra lateral derecha en su tot
     */
    public void infoChanged(){
        channelChanged();
        if(primerCop) {
            mainView.removeInfoCanal();
        }
        loadRightInfo();
        if(mainView.isVisible()) {
            mainView.repaint();
            mainView.revalidate();
        }
    }

    /**
     * Setter del controlador
     * @param controller controlador
     */
    public void setController(MainController controller){
        this.controller = controller;
    }
}
