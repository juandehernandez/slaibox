package controller.cvcontroller;

import model.MainModel;
import network.ServerCommunication;
import network.packet.File;
import util.FileUtils;
import view.MainView;
import view.center.ImagePanel;
import view.center.MessageView;
import view.right.FilePanel;
import view.right.InfoCanal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Controlador del raton para el chat
 *
 * @author Daniel
 * @version 1.2.3
 */
public class CVMouseController extends MouseAdapter implements AdjustmentListener {

    private MainView mainView;
    private MainModel model;
    private ServerCommunication serverCom;

    /**
     * Constructir
     * @param mainView vista principal
     * @param model modelo
     * @param serverCom comunicacion con el servidor
     */
    public CVMouseController(MainView mainView, MainModel model, ServerCommunication serverCom) {
        this.mainView = mainView;
        this.model = model;
        this.serverCom = serverCom;
    }

    /**
     * Gestiona los eventos cuando se les da click a multiples componentes
     *
     * @param e evento
     */
    @Override
    public void mouseClicked(MouseEvent e) {

        if(e.getSource() instanceof JLabel ){
            InfoCanal infoCanal = mainView.getInfoCanal();
            mainView.setInfoCanalVisible(true);
            infoCanal.setUsuarisInfo();
        }else if(e.getSource() instanceof FilePanel){

            FilePanel fp = (FilePanel) e.getSource();

            String ccName = model.getCurrentChannel().getName();
            int idChannel = model.getCurrentChannel().getId();
            boolean cPrivate = model.getCurrentChannel().isPrivate();
            int channelId = model.getCurrentChannel().getId();
            int fileId = fp.getIdMessage();

            //buscar el file en el modelo
            File f = model.getFile(idChannel, fileId,cPrivate);
            // si no es null se encontro el archivo
            if (f != null){
                FileUtils.donwloadFile(f, mainView);
                //Si no esta en el modelo hay que buscarlo en el servidor
            }else {
                serverCom.requestFile(fileId,channelId,ccName);
            }
        }else if (e.getSource() instanceof ImagePanel){

            ImagePanel imagePanel = (ImagePanel) e.getSource();

            String ccName = model.getCurrentChannel().getName();
            boolean cPrivate = model.getCurrentChannel().isPrivate();
            int channelId = model.getCurrentChannel().getId();


            int fileId = imagePanel.getIdMessage();

            //buscar el file en el modelo
            File f = model.getFile(channelId, fileId, cPrivate);
            // si no es null se encontro el archivo
            if (f != null){
                FileUtils.donwloadFile(f, mainView);
                //Si no esta en el modelo hay que buscarlo en el servidor
            }else {
                serverCom.requestFile(fileId,channelId,ccName);
            }

        }
    }


    /**
     * Gestiona cuando se pasa por encima con el raton de multiples componentes
     *
     * @param e evento
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        MessageView message = null;
        Point mouseLocation = null;

        if (e.getSource() instanceof MessageView) {
            message = (MessageView) e.getSource();
            mouseLocation = message.getMousePosition();
        }else if (e.getSource() instanceof JTextArea) {
            JTextArea textArea = (JTextArea) e.getSource();
            mouseLocation = textArea.getMousePosition();
            message = (MessageView) textArea.getParent().getParent();
        }else if (e.getSource() instanceof ImagePanel) {
            ImagePanel imagePanel = (ImagePanel) e.getSource();
            mouseLocation = imagePanel.getMousePosition();
            if (mouseLocation != null){
                imagePanel.showTimePopUp(mouseLocation);
            }
        }
        if (message != null && mouseLocation != null) {
            message.showTimePopUp(mouseLocation);
        }
    }

    /**
     * Gestiona cuando se sale de multiples componentes con el raton en el chat view
     *
     * @param e evento
     */
    @Override
    public void mouseExited(MouseEvent e) {
        MessageView message = null;
        if (e.getSource() instanceof MessageView) {
            message = (MessageView) e.getSource();

        }else if (e.getSource() instanceof JTextArea) {
            JTextArea textArea = (JTextArea) e.getSource();
            message = (MessageView) textArea.getParent().getParent();
        }else if (e.getSource() instanceof ImagePanel){
            ImagePanel imagePanel = (ImagePanel) e.getSource();
            imagePanel.hideTimePopUp();
        }
        if (message != null) {
            message.hideTimePopUp();
        }
    }

    /**
     * Gestiona cuando se utiliza la barra para hacer scroll del chat
     *
     * @param e evento
     */
    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        // Check if user has done dragging the scroll bar
        if(!e.getValueIsAdjusting()){
            JScrollBar scrollBar = (JScrollBar) e.getAdjustable();
            int extent = scrollBar.getModel().getExtent();
            int maximum = scrollBar.getModel().getMaximum();
            if(extent + e.getValue() == maximum){

            }
        }
    }
}
