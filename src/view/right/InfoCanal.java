
package view.right;

import controller.cvcontroller.CVButtonController;
import controller.cvcontroller.CVMouseController;
import network.packet.Profile;
import util.FileUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase InfoCanal
 *
 * Esta clase se encarga de mostrar la información referente a un canal o un chat. Se muestra información general del
 * canal, usuarios del canal y archivos.
 *
 * @author Miguel Abellán
 *
 * @version 2.2
 */
public class InfoCanal extends JPanel{

    private JLabel jlDescripcio;
    private JLabel jlData;
    private JLabel jlNumMissatges;
    private JButton jbImage;
    private JLabel jlEstadoUsuario;
    private JLabel jlNombreUsuario;
    private JPanel jpInformacio;
    private JTabbedPane jTabbedPane;
    private JPanel jpArchivos;
    private JPanel jpUsuariosTotales;
    private JLabel jlNumUsers;
    private JLabel jlNomCanal;
    private JPanel jpUsuari;

    private CVButtonController controller;
    private CVMouseController mouseListener;

    private static final String NUEVO_CHAT = "nuevo_chat";

    /* Color Constant */
    private static final Color COLOR_LATERAL = new Color(100,70,100);
    private static final Color COLOR_LIGHT_WHITE = new Color(245,235,245);
    private static final Color COLOR_LIGHT_GRAY = new Color(230,220,230);

    /**
     * Constructor de la clase
     */
    public InfoCanal() {
        this.setBackground(COLOR_LATERAL);
        setLayout(new BorderLayout());
        jTabbedPane = new JTabbedPane();

        jpInformacio = new JPanel();
        jpInformacio.setLayout(new BoxLayout(jpInformacio, BoxLayout.PAGE_AXIS));

        JPanel jpNomCanal = new JPanel();
        jpNomCanal.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlNomCanal = new JLabel();
        jlNomCanal.setText("Nombre del canal");
        jpNomCanal.add(jlNomCanal);
        jpNomCanal.setBackground(COLOR_LIGHT_WHITE);

        JPanel jpDescripcio = new JPanel();
        jpDescripcio.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlDescripcio = new JLabel();
        jlDescripcio.setText("Aquí va la descripción completa del canal...");
        jpDescripcio.add(jlDescripcio);
        jpDescripcio.setBackground(COLOR_LIGHT_WHITE);

        JPanel jpData = new JPanel();
        jpData.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlData = new JLabel();
        jlData.setText("Fecha de creacion del canal: 01/01/2017");
        jlData.setFont(new Font(jlData.getFont().getName(), Font.PLAIN, 10));
        jpData.add(jlData);
        jpData.setBackground(COLOR_LIGHT_WHITE);

        JPanel jpNumMissatges = new JPanel();
        jpNumMissatges.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlNumMissatges = new JLabel();
        jlNumMissatges.setText("Mensajes enviados: 0");
        jpNumMissatges.add(jlNumMissatges);
        jpNumMissatges.setBackground(COLOR_LIGHT_WHITE);

        jpInformacio.add(jpNomCanal);
        jpInformacio.add(jpDescripcio);
        jpInformacio.add(jpData);
        jpInformacio.add(jpNumMissatges);

        jTabbedPane.addTab("Información", jpInformacio);


        jpUsuariosTotales = new JPanel();
        jpUsuariosTotales.setLayout(new BoxLayout(jpUsuariosTotales, BoxLayout.PAGE_AXIS));

        JScrollPane jScrollPane = new JScrollPane(jpUsuariosTotales);
        jScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        jTabbedPane.addTab("Usuarios", jScrollPane);

        jpArchivos = new JPanel();
        jpArchivos.setLayout(new BoxLayout(jpArchivos, BoxLayout.PAGE_AXIS));

        JScrollPane jScrollPane2 = new JScrollPane(jpArchivos);
        jScrollPane2.getVerticalScrollBar().setUnitIncrement(16);
        jTabbedPane.addTab("Archivos",  jScrollPane2);
        this.setPreferredSize(new Dimension(250,0));
        jTabbedPane.setVisible(true);
        this.add(jTabbedPane, BorderLayout.CENTER);
    }

    /**
     * Muestra la pestaña de la informacion del canal
     */
    public void setChannelInfo(){
        jTabbedPane.setSelectedComponent(jpInformacio);
    }

    /**
     * Muestra la pestaña de la informacion de los usuarios
     */
    public void setUsuarisInfo() {
        jTabbedPane.setSelectedIndex(1);
    }

    /**
     * Muestra la pestaña de la informacion de los archivos
     */
    public void setFilesInfo(){
        jTabbedPane.setSelectedIndex(2);
    }

    /**
     * Devuelve que pestaña esta seleccionada
     * @return pestaña seleccionada
     */
    public int getSelectedTab() {
        return isVisible() ? jTabbedPane.getSelectedIndex() : -1;
    }

    /**
     * Pone el nombre del canal en la informacion del canal
     * @param name
     */
    public void setNameChannel(String name) {
        jlNomCanal.setText(name);
    }

    /**
     * Pone la descripcion del canal en la informacion del canal
     * @param descriptionChannel
     */
    public void setDescriptionChannel(String descriptionChannel) {
        jlDescripcio.setText(descriptionChannel);
    }

    /**
     * Pone la fecha en la informacion del canal
     * @param data
     */
    public void setFechaChannel(String data) {
        jlData.setText("Fecha de creacion del canal: " + data);
    }

    /**
     * Pone el numero de mensajes en la informacion del canal
     * @param num
     */
    public void setNumMissatges(int num) {
        jlNumMissatges.setText("Mensajes enviados: " + num);
    }

    /**
     * Muestra los usuarios que forman parte de ese Channel.
     *
     * @param profiles ArrayList de Profiles con toda su información.
     */
    public void setUsuariInfo(ArrayList<Profile> profiles,String miEmail){
        JPanel jpNum = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel jlUsers = new JLabel("Usuarios totales: ");
        jlNumUsers = new JLabel();
        jpNum.add(jlUsers);
        jpNum.add(jlNumUsers);
        jpNum.setMaximumSize(new Dimension(500,35));
        jpUsuariosTotales.add(jpNum);
        jlNumUsers.setText(Integer.toString(profiles.size()));
        for (int i = 0; i < profiles.size(); i++) {

            jpUsuari = new JPanel();
            jpUsuari.setLayout(new FlowLayout(FlowLayout.LEFT));
            jbImage = new JButton();
            jbImage.setLayout(new FlowLayout(FlowLayout.LEFT));

            jbImage.setPreferredSize(new Dimension(55,55));

            try {
                jbImage.setIcon(new ImageIcon(profiles.get(i).getProfileImage().getImage().getScaledInstance(50,50, Image.SCALE_SMOOTH)));
            } catch (Exception e) {
                System.out.println("No s'ha pogut carregar la imatge.");
            }

            jbImage.setBackground(Color.WHITE);
            jbImage.setOpaque(true);
            jbImage.setContentAreaFilled(true);
            jbImage.setBorderPainted(false);

            jpUsuari.add(jbImage);


            JPanel jpEstadoUsuario = new JPanel();
            jpEstadoUsuario.setLayout(new GridLayout(2,1));
            jlNombreUsuario = new JLabel(profiles.get(i).getUsername());
            jlEstadoUsuario = new JLabel();
            jlEstadoUsuario.setText(profiles.get(i).getStatus());
            jpEstadoUsuario.add(jlNombreUsuario);
            jpEstadoUsuario.add(jlEstadoUsuario);

            JButton jbNuevoChat = new JButton();
            jbNuevoChat.setName(profiles.get(i).getEmail());
            addController(jbNuevoChat); //registra el button listener
            jbNuevoChat.setName(profiles.get(i).getEmail());
            if(i%2 == 0){
                jpUsuari.setBackground(COLOR_LIGHT_GRAY);
                jpEstadoUsuario.setBackground(COLOR_LIGHT_GRAY);
            }else{
                jpUsuari.setBackground(COLOR_LIGHT_WHITE);
                jpEstadoUsuario.setBackground(COLOR_LIGHT_WHITE);
            }
            jbNuevoChat.setBackground(jpUsuari.getBackground());
            jbNuevoChat.setForeground(Color.WHITE);
            jbNuevoChat.setOpaque(true);
            jbNuevoChat.setBorderPainted(false);
            jpUsuari.add(jpEstadoUsuario);
            try {
                jbNuevoChat.setIcon(new ImageIcon((ImageIO.read(
                        new File("resources/chat_icon.png"))).getScaledInstance(40, 40, Image.SCALE_SMOOTH)));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(!miEmail.equals(profiles.get(i).getEmail())) {
                jpUsuari.add(jbNuevoChat);
                addController(jbImage);
                jbImage.setName(profiles.get(i).getEmail());
            }
            jpUsuariosTotales.add(jpUsuari);
        }
    }

    /**
     * Crea la vista de la información referente a los archivos que se han enviado por el grupo.
     *
     * @param files ArrayList de Files, con toda su información.
     */
    public void setFilesInfoView(ArrayList<network.packet.File> files) {

        for(int i = 0; i < files.size(); i++) {

            FilePanel jpArchivo = new FilePanel(files.get(i).getIdChannel(), files.get(i).getMessageId());
            jpArchivo.setLayout(new FlowLayout(FlowLayout.LEFT));
            JPanel jpArchivoZip = new JPanel();
            jpArchivoZip.setLayout(new FlowLayout(FlowLayout.LEFT));
            if(FileUtils.isPicture(files.get(i).getName())) {
                try {
                    jpArchivoZip.add(new JLabel(new ImageIcon(FileUtils.getImageBufferedImage(files.get(i)).getScaledInstance(40,
                            40, Image.SCALE_SMOOTH))));
                } catch (Exception e) {
                    System.out.println("No s'ha pogut carregar la imatge.");
                }
            } else {
                jpArchivoZip.add(new JLabel(new ImageIcon(FileUtils.getFileBufferedImage(files.get(i)).getScaledInstance(25,
                        25, Image.SCALE_SMOOTH))));
            }
            JPanel jpInfoFile = new JPanel(new GridLayout(3,1));
            JLabel jlNombreArchivo = new JLabel();
            jlNombreArchivo.setText(files.get(i).getName());
            JLabel jlData = new JLabel(files.get(i).getDate().toString());
            JLabel jlUser = new JLabel(files.get(i).getEmailUser());

            if(i%2 == 0){
                jpInfoFile.setBackground(COLOR_LIGHT_GRAY);
                jlData.setBackground(COLOR_LIGHT_GRAY);
                jlUser.setBackground(COLOR_LIGHT_GRAY);
                jpArchivo.setBackground(COLOR_LIGHT_GRAY);
                jlNombreArchivo.setBackground(COLOR_LIGHT_GRAY);
            }else{
                jlData.setBackground(COLOR_LIGHT_WHITE);
                jlUser.setBackground(COLOR_LIGHT_WHITE);
                jpInfoFile.setBackground(COLOR_LIGHT_WHITE);
                jpArchivo.setBackground(COLOR_LIGHT_WHITE);
                jlNombreArchivo.setBackground(COLOR_LIGHT_WHITE);
            }

            jpInfoFile.add(jlNombreArchivo);
            jpInfoFile.add(jlUser);
            jpInfoFile.add(jlData);

            jpArchivo.add(jpArchivoZip);
            jpArchivo.add(jpInfoFile);
            jpArchivo.addMouseListener(mouseListener);
            jpArchivos.add(jpArchivo);

        }
    }

    /**
     * Registra los controladores a la vista
     * @param actionListener
     * @param mouseListener
     */
    public void registerController(ActionListener actionListener, MouseListener mouseListener) {
        controller = (CVButtonController) actionListener;
        this.mouseListener = (CVMouseController) mouseListener;
    }

    /**
     * agrega el Button controller a los botones
     */
    private void addController(JButton jbNuevoChat){
        jbNuevoChat.addActionListener(controller);
        jbNuevoChat.setActionCommand(NUEVO_CHAT);
    }

    /**
     * Elimina la información que hay en las vistas.
     */
    public void removeAllInfoRight(){
        jpArchivos.removeAll();
        jpArchivos.revalidate();
        jpArchivos.repaint();
        jpUsuariosTotales.removeAll();
        jpUsuariosTotales.revalidate();
        jpUsuariosTotales.repaint();
    }
}
