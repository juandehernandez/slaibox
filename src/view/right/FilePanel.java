package view.right;

import javax.swing.*;

/**
 * Created by MatiasVillarroel on 16/05/17.
 */
public class FilePanel extends JPanel {

    private int idChannel;
    private int idMessage;

    /**
     * Constructor de File Panel
     * @param idChannel id del canal del archivo que se añade a un FilePanel
     * @param idMessage id del mensaje del archivo que se añade a un FilePanel
     */
    public FilePanel(int idChannel, int idMessage){
        this.idChannel = idChannel;
        this.idMessage = idMessage;
    }

    /**
     * Devuelve el id del canal guardado en el panel
     * @return el id del canal guardado en el panel
     */
    public int getIdChannel() {
        return idChannel;
    }

    /**
     * Devuelve el id del mensaje guardado en el panel
     * @return el id del mensaje guardado en el panel
     */
    public int getIdMessage() {
        return idMessage;
    }

}
