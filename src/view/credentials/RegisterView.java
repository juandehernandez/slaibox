package view.credentials;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Vista de registro de SlaiBox
 *
 * @author Daniel Ortiz
 * @version 1.2.0
 */
public class RegisterView extends JFrame {

	private JTextField txtUsername;
	private JTextField txtEmail;
	private JPasswordField txtPassword;
	private JPasswordField txtConfirm;
	private JButton btnRegister;

	/**
	 * Constructor de la clase
	 */
	public RegisterView() {
		setResizable(true);
		setTitle("SlaiBox Register");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(450, 260));
		setResizable(false);
		setLocationRelativeTo(null);
		JPanel contentPane = (JPanel) getContentPane();
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		/* Establecemos el icono */
		try {
			Image image = ImageIO.read(new File("resources/logo.png"));
			setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Could not load the icon of the window.");
		}

		final JPanel jpTitle = new JPanel();
		final JPanel jpLogo = new JPanel();
		jpLogo.setPreferredSize(new Dimension(130,50));
		try {
			jpLogo.add(new JLabel((new ImageIcon(
					((BufferedImage)ImageIO.read(new File("resources/titol.png"))).getScaledInstance(140, 40,
							Image.SCALE_SMOOTH)))));
		} catch (Exception e) {
			System.out.println("Could not load the image.");
		}
		jpLogo.setBorder(new EmptyBorder(5,5,5,5));
		final JPanel jpFlowTitle = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpFlowTitle.setBorder(new EmptyBorder(0,0,5,0));
		JLabel lblTitle = new JLabel("Register: ");
		lblTitle.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		BoxLayout boxLayout = new BoxLayout(jpTitle,BoxLayout.PAGE_AXIS);
		jpTitle.setLayout(boxLayout);
		jpFlowTitle.add(lblTitle);
		jpTitle.add(jpLogo);
		jpTitle.add(jpFlowTitle);
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(jpTitle, BorderLayout.NORTH);
		
		JPanel pnlMain = new JPanel();
		contentPane.add(pnlMain, BorderLayout.CENTER);
		pnlMain.setLayout(new BoxLayout(pnlMain, BoxLayout.Y_AXIS));
		
		JPanel pnlUsername = new JPanel();
		pnlUsername.setLayout(new BoxLayout(pnlUsername, BoxLayout.X_AXIS));
		pnlMain.add(pnlUsername);

		JLabel lblUsername = new JLabel("Username: ");
		lblUsername.setPreferredSize(new Dimension(150,20));
		pnlUsername.add(lblUsername);
		txtUsername = new JTextField();
		pnlUsername.add(txtUsername);
		
		Dimension d = new Dimension(Integer.MAX_VALUE, txtUsername.getPreferredSize().height);
		txtUsername.setMaximumSize(d);
		
		JPanel pnlEmail = new JPanel();
		pnlMain.add(pnlEmail);
		pnlEmail.setLayout(new BoxLayout(pnlEmail, BoxLayout.X_AXIS));
		
		JLabel lblEmail = new JLabel("Email: ");
		lblEmail.setPreferredSize(new Dimension(150,20));
		pnlEmail.add(lblEmail);
		
		txtEmail = new JTextField();
		pnlEmail.add(txtEmail);
		
		txtEmail.setMaximumSize(d);
		
		JPanel pnlPassword = new JPanel();
		pnlMain.add(pnlPassword);
		pnlPassword.setLayout(new BoxLayout(pnlPassword, BoxLayout.X_AXIS));
		
		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setPreferredSize(new Dimension(150,20));
		pnlPassword.add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setMaximumSize(d);
		pnlPassword.add(txtPassword);

		
		JPanel pnlConfirm = new JPanel();
		pnlConfirm.setBorder(new EmptyBorder(0,0,5,0));
		pnlMain.add(pnlConfirm);
		pnlConfirm.setLayout(new BoxLayout(pnlConfirm, BoxLayout.X_AXIS));
		
		JLabel lblConfirm = new JLabel("Confirm password: ");
		lblConfirm.setPreferredSize(new Dimension(150,20));
		pnlConfirm.add(lblConfirm);

		txtConfirm = new JPasswordField();
		txtConfirm.setMaximumSize(d);
		pnlConfirm.add(txtConfirm);
		
		btnRegister = new JButton("Register");
		contentPane.add(btnRegister, BorderLayout.SOUTH);
	}

	/**
	 * Devuelve el username a registrar
	 * @return username a registrar
	 */
	public String getUsername() {
	    return txtUsername.getText();
    }

	/**
	 * Devuelve el email a registrar
	 * @return email a registrar
	 */
	public String getEmail() {
	    return txtEmail.getText();
    }

	/**
	 * Devuelve la contraseña a registrar
	 * @return contraseña a a registrar
	 */
	public char[] getPassword() {
	    return txtPassword.getPassword();
    }

	/**
	 * Devuelve la confirmacion de la contraseña a registrar
	 * @return confirmacion de la contraseña a registrar
	 */
	public char[] getConfirmedPassword() {
	    return txtConfirm.getPassword();
    }

	/**
	 * Registra el controlador a la vista
	 * @param ml usado para detectar los clicks
	 * @return una vista de registro
	 */
	public RegisterView registerController(MouseListener ml) {
        btnRegister.addMouseListener(ml);
        return this;
    }

	/**
	 * Usado para definir las restricciones del GridBagLayout
	 * @param gbc grid bag constraints
	 * @param weight peso que tiene el componente
	 * @param gridx posicion horizontal
	 * @param gridy posicion vertical
	 * @param fill espacio que ocupan
	 * @return grid bag constraints
	 */
    private GridBagConstraints setConstraints(GridBagConstraints gbc,float weight, int gridx, int gridy,int fill){
		gbc.weightx = weight;
		gbc.fill = fill;
		gbc.gridx = gridx;
		gbc.gridy = gridy;
		return gbc;
	}
}
