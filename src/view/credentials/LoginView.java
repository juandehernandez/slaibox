package view.credentials;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

/**
 * Vista de login de SlaiBox
 *
 * @author Daniel Ortiz
 * @version 1.2.0
 */
public class LoginView extends JFrame {

	private JTextField txtUser;
	private JPasswordField txtPassword;
	private JButton btnLogin;
	private JButton btnRegister;
	private final Dimension size;

    private static final String NO_REGISTER = "You don't have an account? Register: ";
	private static final String LOGIN = "Log In";
	private static final String REGISTER = "Register";

	/**
	 * Constructor de la vista
	 */
	public LoginView() {
	    size = new Dimension(120, 70);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(400, 270));
        ((BorderLayout) getLayout()).setVgap(5);
		setTitle("SlaiBox Login");
		setLocationRelativeTo(null);
		JPanel contentPane = (JPanel) getContentPane();
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		try {
			Image image = ImageIO.read(new File("resources/logo.png"));
			setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Could not load the icon of the window.");
		}

        final JPanel jpTitle = new JPanel();
        final JPanel jpLogo = new JPanel();
        jpLogo.setPreferredSize(new Dimension(130,50));
        try {
            jpLogo.add(new JLabel((new ImageIcon(
                    (ImageIO.read(new File("resources/titol.png"))).getScaledInstance(140, 40,
                            Image.SCALE_SMOOTH)))));
        } catch (Exception e) {
            System.out.println("Could not load the image.");
        }
        jpLogo.setBorder(new EmptyBorder(5,5,5,5));
        final JPanel jpFlowTitle = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jpFlowTitle.setBorder(new EmptyBorder(0,0,5,0));
        JLabel lblTitle = new JLabel("Login:");
        lblTitle.setFont(new Font("Lucida Grande", Font.BOLD, 20));
        BoxLayout boxLayout = new BoxLayout(jpTitle,BoxLayout.PAGE_AXIS);
        jpTitle.setLayout(boxLayout);
        jpFlowTitle.add(lblTitle);
        jpTitle.add(jpLogo);
        jpTitle.add(jpFlowTitle);
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(jpTitle, BorderLayout.NORTH);

		JPanel pnlMain = new JPanel();
		contentPane.add(pnlMain, BorderLayout.CENTER);
		pnlMain.setLayout(new BoxLayout(pnlMain, BoxLayout.Y_AXIS));

		pnlMain.setMinimumSize(new Dimension(300, 150));
		JPanel pnlUser = new JPanel();
		pnlMain.add(pnlUser);
		pnlUser.setLayout(new BoxLayout(pnlUser, BoxLayout.X_AXIS));

		JLabel lblUserEmail = new JLabel("Username/Email:");
		lblUserEmail.setPreferredSize(size);
		pnlUser.add(lblUserEmail);

		txtUser = new JTextField();
		pnlUser.add(txtUser);
		Dimension d = new Dimension(Integer.MAX_VALUE, txtUser.getPreferredSize().height);
		txtUser.setMaximumSize(d);

		JPanel pnlPassword = new JPanel();
		pnlMain.add(pnlPassword);
		pnlPassword.setLayout(new BoxLayout(pnlPassword, BoxLayout.X_AXIS));

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setPreferredSize(size);
		pnlPassword.add(lblPassword);

		txtPassword = new JPasswordField();
		pnlPassword.add(txtPassword);
		txtPassword.setMaximumSize(d);

		final JPanel jpBottomButtons = new JPanel();
		BoxLayout boxLayout1 = new BoxLayout(jpBottomButtons,BoxLayout.PAGE_AXIS);
		jpBottomButtons.setLayout(boxLayout1);
		btnLogin = new JButton(LOGIN);
		jpBottomButtons.add(btnLogin);

		final JLabel jlRegister = new JLabel(NO_REGISTER);
		final JPanel jpRegisterButton = new JPanel();
		jpRegisterButton.setBorder(new EmptyBorder(5,0,5,0));
		jpRegisterButton.add(jlRegister);
		jpBottomButtons.add(jpRegisterButton);
		btnRegister = new JButton(REGISTER);
		jpBottomButtons.add(btnRegister);
		contentPane.add(jpBottomButtons, BorderLayout.SOUTH);
	}


	/**
	 * Devuelve el email/username del usaurio que se esta loggeando
	 * @return email/username del usaurio que se esta loggeando
	 */
	public String getUser() {
		return txtUser.getText();
	}

	/**
	 * Devuelve contraseña del usaurio que se esta loggeando
	 * @return contraseña del usaurio que se esta loggeando
	 */
	public char[] getPassword() {
		return txtPassword.getPassword();
	}

	/**
	 * Registra los controladores
	 * @param ml detecta que botones se clickean
	 * @param kl detecta el uso del enter
	 * @return
	 */
	public LoginView registerController(MouseListener ml, KeyListener kl) {
		btnLogin.addMouseListener(ml);
		btnRegister.addMouseListener(ml);
        txtPassword.addKeyListener(kl);
        txtUser.addKeyListener(kl);
        return this;
	}

	/**
	 * Des habilita el boton de login y registro
	 */
	public void unableLoginButton(){
		btnLogin.setEnabled(false);
		btnRegister.setEnabled(false);
	}

	/**
	 * Habilita los botones de login y registro
	 */
	public void enableLoginButton(){
		btnLogin.setEnabled(true);
		btnRegister.setEnabled(true);
	}

	/**
	 * Indica al boton de login que haga la accion de click
	 */
	public void pressLogin() {
		btnLogin.doClick();
	}

	/**
	 * Devuelve el boton de login
	 * @return boton de login
	 */
    public JButton getBtnLogin() {
        return btnLogin;
    }
}
