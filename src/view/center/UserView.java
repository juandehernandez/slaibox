package view.center;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Vista de la informacion del Usuario actual
 * @author miquelabellan
 * @version 2.1.0
 */
public class UserView extends JPanel {

    private JButton jbLogout;
    private JRadioButton jrbEstatAvailable;
    private JRadioButton jrbEstatAway;
    private JLabel jlEstat;
    private JLabel jlbNomUsuari;
    private JLabel jlEmail;
    private JPanel jpImage;

    private boolean imageSet;

    public static final String CONNECTED = "Connected";
    public static final String OCCUPIED = "Occupied";
    public static final String LOGOUT = "LOGOUT";

    /**
     * Constructor de la clase
     * @param nomUsuari nombre del usuario
     */
    public UserView(String nomUsuari){

        this.setLayout(new GridBagLayout());
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.setBackground(Color.BLUE);

        jpImage = new JPanel();
        jpImage.setLayout(new FlowLayout(FlowLayout.CENTER));

        imageSet = false;

        jPanel.add(jpImage);
        JPanel jpNomUsuari = new JPanel();
        jpNomUsuari.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlbNomUsuari = new JLabel();
        jlbNomUsuari.setText(nomUsuari);
        jlEmail = new JLabel();
        jpNomUsuari.add(jlbNomUsuari);
        jpNomUsuari.add(jlEmail);

        JPanel jpEstatUsuari = new JPanel();
        jpEstatUsuari.setLayout(new FlowLayout(FlowLayout.CENTER));
        jlEstat = new JLabel();
        jlEstat.setText("Available");
        jpEstatUsuari.add(jlEstat);

        ButtonGroup jpRadioGroup = new ButtonGroup();
        //jpRadioGroup.setLayout(new FlowLayout(FlowLayout.CENTER));
        jrbEstatAvailable = new JRadioButton();
        jrbEstatAvailable.setText(CONNECTED);
        jrbEstatAway = new JRadioButton();
        jrbEstatAway.setText(OCCUPIED);

        jpRadioGroup.add(jrbEstatAvailable);
        jpRadioGroup.add(jrbEstatAway);

        jrbEstatAvailable.setSelected(true);

        JPanel jpBoton = new JPanel();
        jpBoton.setLayout(new FlowLayout(FlowLayout.CENTER));
        jbLogout = new JButton();
        jbLogout.setText("Logout");
        jpBoton.add(jbLogout);

        final JPanel jpGroup = new JPanel();
        jpGroup.add(jrbEstatAvailable);
        jpGroup.add(jrbEstatAway);
        jPanel.add(jpNomUsuari);
        jPanel.add(jpEstatUsuari);
        jPanel.add(jpGroup);
        jPanel.add(jpBoton);

        jPanel.setVisible(true);


        this.add(jPanel);

    }

    /**
     * Registra los controladores a la vista
     * @param actionListener
     */
    public void registraControlador(ActionListener actionListener){
        jrbEstatAvailable.addActionListener(actionListener);
        jrbEstatAvailable.setActionCommand(CONNECTED);

        jrbEstatAway.addActionListener(actionListener);
        jrbEstatAway.setActionCommand(OCCUPIED);

        jbLogout.addActionListener(actionListener);
        jbLogout.setActionCommand(LOGOUT);
    }

    /**
     * Set el nombre del usuario en la vista
     * @param username nombre del usuario que aparece en la vista
     */
    public void setUsername(String username){
        jlbNomUsuari.setText(username);
    }

    /**
     * Set el email del usuario.
     * @param email email del usuario que aparece en la vista
     */
    public void setEmail(String email){
        jlEmail.setText(email);
    }

    /**
     * Set la imagen de perfil del usuario
     * @param profileImage imagen de perfil del usuario
     */
    public void setProfileImage(ImageIcon profileImage){
        if(!imageSet) {
            jpImage.add(new JLabel(new ImageIcon(profileImage.getImage()
                                    .getScaledInstance(100,100,Image.SCALE_SMOOTH))));
            imageSet = true;
        }
    }

    /**
     * Set del estado del usuario
     * @param status estado del usuario
     */
    public void setStatus(String status){
        jlEstat.setText(status);
    }

}
