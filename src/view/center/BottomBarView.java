package view.center;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Vista inferior del Chat del cliente
 * @author Matias
 * @version 2.1.0
 */
public class BottomBarView extends JPanel {

    private static final Color COLOR_BOTOM_LIGHT = new Color(130,100,130);

    private final JTextField jtfMessage;
    private final JButton jbSend;
    private final JButton jbAddAttachment;

    /**
     * Crea la vista de la barra de abajo donde se escriben los mensajes
     */
    public BottomBarView(){

        setLayout(new BorderLayout(5, 0));
        setBorder(new EmptyBorder(20, 20, 20, 20));

        /*inicializar el textField*/
        jtfMessage = new JTextField();

        /*crear el contenedor de botones*/
        GridLayout gl = new GridLayout(1,2);
        gl.setHgap(5);
        JPanel jpButtonContainer = new JPanel();
        jpButtonContainer.setLayout(gl);

        /*crear los botones*/
        jbSend = new JButton("SEND");
        jbSend.setForeground(Color.WHITE);
        jbSend.setBackground(COLOR_BOTOM_LIGHT);
        jbSend.setOpaque(true);
        jbSend.setContentAreaFilled(true);
        jbSend.setBorderPainted(false);

        /*añadir imagen al boton de añadir archivos*/
        jbAddAttachment = new JButton();
        try {
            ImageIcon img = new ImageIcon(
                    ((BufferedImage) ImageIO.read(new File("resources/clip_icon.png"))).
                                        getScaledInstance(20, 20, Image.SCALE_SMOOTH));
            jbAddAttachment.setIcon(img);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        jbAddAttachment.setBackground(COLOR_BOTOM_LIGHT);
        jbAddAttachment.setOpaque(true);
        jbAddAttachment.setBorderPainted(false);

        /*añadir los botones al contenedor de botones*/
        jpButtonContainer.add(jbSend);
        jpButtonContainer.add(jbAddAttachment);

        /*añadir los componentes al panel*/
        add(jtfMessage, BorderLayout.CENTER);
        add(jpButtonContainer, BorderLayout.LINE_END);
    }

    /**
     * Registra un ActionListener a los botones
     * @param actionListener
     */
    public void registerController(ActionListener actionListener, KeyListener keyListener, FocusListener focusListener){

        jbSend.setActionCommand("SEND_MESSAGE");
        jbSend.addActionListener(actionListener);

        jbAddAttachment.setActionCommand("ADD_ATTACHMENT");
        jbAddAttachment.addActionListener(actionListener);
        jtfMessage.addFocusListener(focusListener);
        jtfMessage.addKeyListener(keyListener);
        jtfMessage.setActionCommand("TEXT_FIELD");
    }

    /**
     * Devuelve el mensaje que escribe el usuario
     * @return String con el mensaje
     */
    public String getMessageField(){
        return jtfMessage.getText();
    }

    /**
     * Vacia el campo de mensaje
     */
    public void eraseMessageField(){
        jtfMessage.setText(null);
    }

    /**
     * le indica al boton de enviar que se haga click
     */
    public void sendClick() {
        jbSend.doClick();
    }
}
