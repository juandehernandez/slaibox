package view.center;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase para mostrar un mensaje, este puede ser un mensaje normal con solo
 * el texto y la fecha (visible mediante hovering) o un primer mensaje
 *
 * @author Sergi Valbuena
 * @version 01/04/2017
 */
public class MessageView extends JPanel {
    /** Hora del mensaje */
    protected Date date;
    private final JPopupMenu timePopUp;
    private final JPanel messagePanel;
    private final JTextArea messageContent;

    /**
     * Constructor
     * @param msg mensaje a mostrar
     * @param date fecha del mensaje
     */
    public MessageView(String msg, Date date, MouseListener listener) {
        this.date = date;
        setOpaque(false);

        /* BorderLayout para colocar el mensaje en el centro y que ocupe lo maximo,
         * si es un primer mensaje la fotografia, nombre y hora se pondra en la
         * parte superior
         */
        setLayout(new BorderLayout());
        messagePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        messagePanel.setOpaque(false);
        add(messagePanel, BorderLayout.CENTER);

        messageContent = new JTextArea(msg);
        messageContent.addMouseListener(listener);
        messageContent.setOpaque(false);
        messagePanel.add(messageContent);
        timePopUp = new JPopupMenu();
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String hour = "Sent at " + formatter.format(date.getTime());
        timePopUp.add(new JMenuItem(hour));

        addMouseListener(listener);
    }

    /**
     * Muestra el pop up con la fecha en la que se escribio el mensaje
     * @param location posicion en la pantalla
     */
    public void showTimePopUp(Point location) {
        timePopUp.show(this, location.x + 10, location.y + 5);
    }

    /**
     * Esconde el pop up con la fecha en la que se escribio el mensaje
     */
    public void hideTimePopUp() {
        timePopUp.setVisible(false);
    }

    /**
     * Retorna el panel de mansajes
     * @return panel de mensajes
     */
    public JPanel getMessagePanel() {
        return messagePanel;
    }

    /**
     * Retorna el contenido del mensaje
     * @return contenido del mensaje
     */
    public JTextArea getMessageContent() {
        return messageContent;
    }
}
