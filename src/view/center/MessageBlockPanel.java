package view.center;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * Panel que contiene el bloque, necesario para pintar el fondo porque
 * no se puede pintar un panel con box layout
 *
 * @author Sergi Valbuena
 * @version 02/04/2017
 */
public class MessageBlockPanel extends JPanel {
    /**
     * currentBlock es una box donde se apilan paneles como MessageView
     */
    private JPanel currentBlock;
    private Color backgroundColor;

    /**
     * Constructor de la clase
     */
    public MessageBlockPanel(Color color) {
        setOpaque(false);
        setLayout(new FlowLayout(FlowLayout.LEADING));
        /* Creamos el panel para el nuevo bloque, es una box de paneles de mensaje */
        currentBlock = new JPanel();
        currentBlock.setLayout(new BoxLayout(currentBlock, BoxLayout.PAGE_AXIS));
        currentBlock.setName("CURRENTBLOCK2");
        add(currentBlock);
        setName("MESSAGEBLOCKPANEL");
        currentBlock.setOpaque(false);
        backgroundColor = color;
    }

    /**
     * Añade un mensaje al bloque
     * @param msg texto del mensaje
     * @param date fecha del mensaje
     * @param listener MouseListener para mostrar la hora al pasar el raton por encima
     */
    public MessageView addMessage(String msg, Date date, MouseListener listener) {
        /* Creamos un mensaje normal nuevo y lo añadimos al bloque actual*/
        MessageView msgView = new MessageView(msg, date, listener);
        msgView.setName(msg);
        currentBlock.add(msgView);
        return msgView;
    }

    /**
     * Añade un primer mensaje en un bloque, solo tiene sentido hacerlo una vez
     * por bloque
     * @param msg texto del mensaje
     * @param username nombre del usuario creador del mensaje
     * @param image ruta a la foto del usuario
     * @param date fecha del mensake
     * @param listener MouseListener para mostrar la hora al pasar el raton por encima
     */
    public MessageView addFirstMessage(String msg, String username, ImageIcon image, Date date, MouseListener listener) {
         /* Creamos el primer mensaje y lo añadimos al messagePanel */
        FirstMessageView msgView = new FirstMessageView(msg, username, image, date, listener);
        msgView.addMouseListener(listener);
        msgView.setName(msg);
        currentBlock.add(msgView);
        return msgView;
    }

    /**
     * Añade una imagen al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     */
    public void addImageToMessage(Date date, BufferedImage image, int idMessage, MouseListener mouseListener) {
        ImagePanel imagePanel = new ImagePanel(image, new Dimension(200, 200),date, idMessage);
        imagePanel.setName("Image");
        imagePanel.addMouseListener(mouseListener);
        currentBlock.add(imagePanel);
    }

    /**
     * Añade un placeholder del file al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     */
    public void addFileToMessage(Date date, BufferedImage image, String filename,int idMessage, MouseListener mouseListener) {
        ImagePanel imagePanel = new ImagePanel(image, new Dimension(100, 100),date, idMessage);
        imagePanel.setName("File");
        imagePanel.addMouseListener(mouseListener);
        currentBlock.add(imagePanel);
        JLabel filenameLabel = new JLabel(filename);
        filenameLabel.setVerticalAlignment(SwingConstants.CENTER);
        currentBlock.add(filenameLabel);
    }

    /**
     * Aqui se pinta el f graficos a mostrar
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(backgroundColor);
        Dimension d = getPreferredSize();
        g.fillRoundRect(0, 0, (int)d.getWidth(), (int)d.getHeight(), 40, 40);
    }
}
