package view.center;

import view.custom_ui.CustomJPanelDateUI;
import view.custom_ui.CustomScrollBarUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Vista del chat, proporciona las funciones para añadir mensajes
 *
 * @author Sergi Valbuena
 * @version 01/04/2017
 */
public class ChatView extends JPanel {

    private static final String ADD_MORE_MESSAGES = "Cargar mas mensajes";

    /**
     * Box donde se pone cada bloque de mensajes
     */
    private JPanel boxPane;
    private JScrollPane scrollPane;
    private JButton moreButton;

    private MessageBlockPanel currentBlock;
    private Image image;
    private boolean imageAlreadyScaled;

    private JPanel notViewedNotificationPanel;
    private JLabel notViewedLabel;
    private int notViewedNumber;

    /*se agregan a paneles que se crean de forma dinamica*/
    private MouseListener mouseListener;
    private AdjustmentListener adjustmentListener;
    private ActionListener actionListener;

    /**
     * Constructor de la vista del chat...
     */
    public ChatView() {
        setLayout(new BorderLayout());
        imageAlreadyScaled = false;


        try {
            image = ImageIO.read(new File("resources/chatBackground.jpeg"));
            Dimension d = this.getPreferredSize();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Borra el contenido del chat
     */
    public void clearChatView() {
        /* Creamos la box de los bloques de mensaje */
        boxPane = new JPanel();
        boxPane.setLayout(new BoxLayout(boxPane, BoxLayout.PAGE_AXIS));
        boxPane.setOpaque(false);
        notViewedNotificationPanel = null;

        /* Creamos el scroll pane donde ira la box de los bloques */
        scrollPane = new JScrollPane(boxPane);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.getHorizontalScrollBar().setUI(new CustomScrollBarUI(new Color(90,60,90),
                                                                    new Color(240,220,240)));
        scrollPane.getVerticalScrollBar().setUI(new CustomScrollBarUI(new Color(90,60,90),
                                                                new Color(240,220,240)));
        add(scrollPane, BorderLayout.CENTER);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setOpaque(false);
        scrollPane.getVerticalScrollBar().addAdjustmentListener(adjustmentListener);
        setName("CHATVIEW CLEAR");

    }

    /**
     * Añade un mensaje al último bloque añadido
     * @param msg mensaje a mostrar
     * @param date fecha del mensaje(se muestra cuando hover raton)
     */
    public void addMessage(String msg, Date date, boolean isMine) {
        MessageView msj = currentBlock.addMessage(msg, date, mouseListener);


        /* Informamos de que se han añadido paneles nuevos en runtime, sino no
         * se actualiza la vista
         */
        revalidate();
    }

    /**
     * Añade un bloque de mensajes nuevo junto con el primer mensaje
     * @param msg mensaje a mostrar
     * @param username nombre del usuario que ha escrito el mensaje
     * @param image imagen de usuario que ha escrtio el mensake
     * @param date fecha del mensaje(aqui si se muestra por pantalla)
     * @param color color del fondo del bloque de mensajes
     */
    public void addFirstMessage(String msg, String username, ImageIcon image, Date date, Color color,boolean isMine) {

        currentBlock = new MessageBlockPanel(color);
        currentBlock.setName("MessageBlock");
        currentBlock.setOpaque(false);

        JPanel paddingPanel = null;
        if(isMine) {
            paddingPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));

        }else{
            paddingPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        }
        paddingPanel.setName("PaddingPanel");
        paddingPanel.setOpaque(false);
        paddingPanel.setBorder(new EmptyBorder(10, 40, 10, 40));
        paddingPanel.add(currentBlock);
        boxPane.add(paddingPanel);
        currentBlock.addFirstMessage(msg, username, image, date, mouseListener);

        /* Informamos de que se han añadido paneles nuevos en runtime, sino no
         * se actualiza la vista
         */
        revalidate();
    }

    /**
     * Añade una imagen al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     */
    public void addImageToMessage(Date date, BufferedImage image, int idMessage) {
        currentBlock.addImageToMessage(date, image, idMessage, mouseListener);

        revalidate();
    }

    /**
     * Añade un placeholder del file al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     * @param filename nombre del fichero
     */
    public void addFileToMessage(Date date, BufferedImage image, String filename, int idMessage) {
        currentBlock.addFileToMessage(date, image, filename, idMessage, mouseListener);

        revalidate();
    }

    /**
     * Añade una notificacion en el chat de que ha cambiado el dia
     * @param day
     */
    public void addNewDayNotification(String day) {
        JPanel newDayPanel = new JPanel();
        newDayPanel.setUI(new CustomJPanelDateUI());
        newDayPanel.setMaximumSize(new Dimension(getWidth(),0));
        newDayPanel.setBackground(new Color(230,230,230));
        JLabel dayLabel = new JLabel(day);
        newDayPanel.add(dayLabel);
        boxPane.add(newDayPanel);
        revalidate();
    }

    /**
     * Añade una notificacion en el chat de que ha cambiado el dia
     * @param msgNumber : numero de mensajes sin leer por debajo de la notificacion
     */
    public void addNotViewedNotification(int msgNumber) {
        notViewedNumber = msgNumber;
        if (notViewedNotificationPanel == null) {
            notViewedNotificationPanel = new JPanel();
            notViewedNotificationPanel.setUI(new CustomJPanelDateUI());
            notViewedNotificationPanel.setMaximumSize(new Dimension(getWidth(),0));
            notViewedNotificationPanel.setBackground(new Color(230,230,230));
            notViewedLabel = new JLabel();
            notViewedNotificationPanel.add(notViewedLabel);
            boxPane.add(notViewedNotificationPanel);
        }
        notViewedLabel.setText("▼ " + notViewedNumber + " messages not read ▼");
	}
	
	/**
     * Añade un boton para pedir mas mensajes
     */
    public void addRequestMoreMessages() {
        JPanel moreMessagesPanel = new JPanel();		
	    moreButton = new JButton(ADD_MORE_MESSAGES);
        moreButton.setActionCommand("LOAD_MORE_MESSAGES");
        moreButton.addActionListener(actionListener);
        moreMessagesPanel.setMaximumSize(moreButton.getPreferredSize());
        moreMessagesPanel.add(moreButton);
        boxPane.add(moreMessagesPanel);
        revalidate();
    }

    /**
     * Añade una notificacion en el chat de que hay mensajes no leidos
     */
    public void addNotReadMessages() {
        addNewDayNotification("Mensajes no leidos");
    }

    /**
     * Borra la notificacion de mensajes no leidos
     */
    public void clearNotViewedNotification() {
        if (notViewedNotificationPanel != null) {
            boxPane.remove(notViewedNotificationPanel);
            notViewedNotificationPanel = null;
            notViewedNumber = 0;
        }
        revalidate();
    }


    /**
     * Prepara el chat view para cargar una tanda de mensajes
     */
    public void prepareChatView() {
        notViewedNotificationPanel = null;
    }

    /**
     * Incrementa el valor de mensajes no vistos
     * @return
     */
    public boolean incrementNotViewedNotification() {
        if (notViewedNotificationPanel == null) {
            notViewedNumber = 1;
            addNotViewedNotification(1);
            return true;
        } else {
            notViewedNumber++;
            notViewedLabel.setText("▼ " + notViewedNumber + " messages not read ▼");
            return false;
        }
    }

    /**
     * Metodo para pintar el fondo al chat, la imagen solo se tiene que escalar
     * una vez y como este metodo se llama constantemente la view tendria lag si
     * no se controlase.
     * @param g graficos a mostrar
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.PINK);
    }

    /**
     * Scrollea hasta el fondo
     */
    public void scrollToBottom() {
        JScrollBar bar = scrollPane.getVerticalScrollBar();
        if(bar != null) {
            bar.setValue(bar.getMaximum());
        }
    }

    /**
     * Devuelve si el scroll esta abajo de todo
     * @return
     */
    public boolean isScrollOnBottom() {
        return (scrollPane.getVerticalScrollBar().getValue() >= (scrollPane.getVerticalScrollBar().getMaximum() - 1000));
    }

    /**
     * Registra un ActionListener a los botones
     * @param mouseListener listener
     * @param adjustmentListener listener
     * @param actionListener listener
     */
	 
    public void registerController(MouseListener mouseListener, AdjustmentListener adjustmentListener, ActionListener actionListener) {
        this.mouseListener = mouseListener;
		this.actionListener = actionListener;
		this.adjustmentListener = adjustmentListener;
    }

    /**
     * Activa o desactiva el boton de cargar mas mensajes
     * @param bool
     */
    public void setEnableRequestMoreMessages(boolean bool){ moreButton.setEnabled(bool); }
}
