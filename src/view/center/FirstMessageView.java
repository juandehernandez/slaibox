package view.center;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase para mostrar un nuevo mensaje por el chat, con una foto, nombre de usuario,
 * fecha y el propio mensaje
 *
 * @author Sergi Valbuena
 * @version 01/04/2017
 */
public class FirstMessageView extends MessageView {

    /**
     * Constructor
     * @param msg mensaje a mostrar
     * @param username nombre de usuario del creador del mensaje
     * @param image imagen del creador del mensaje
     * @param date fecha del mensaje
     */
    public FirstMessageView(String msg, String username, ImageIcon image, Date date, MouseListener listener) {
        /* Creamos el mensaje normal */
        super(msg, date, listener);

        /* Panel donde va la imagen, nombre y hora */
        final JPanel userInfoPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        userInfoPanel.setOpaque(false);
        /* Se coloca en la parte superior del mensaje*/
        add(userInfoPanel, BorderLayout.PAGE_START);

        /* Imagen del usuario */
        final JLabel userImage = new JLabel();
        userImage.setOpaque(true);
        userInfoPanel.add(userImage);

        Image imageAux = image.getImage();
        ImageIcon imageIcon = new ImageIcon(imageAux.getScaledInstance(40, 40,
                                    Image.SCALE_SMOOTH));
        userImage.setIcon(imageIcon);

        /* Nombre del usuario */
        final JLabel usernameLabel = new JLabel(username);
        userInfoPanel.add(usernameLabel);

        /* Hora del mensaje */
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String hour = "Sent at " + formatter.format(date.getTime());
        final JLabel hourLabel = new JLabel(hour);
        userInfoPanel.add(hourLabel);
    }
}