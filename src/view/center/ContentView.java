package view.center;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * Clase de contenido de vista
 *
 * @author Matias
 */
public class ContentView extends JPanel {

    private TopBarView topBarView;
    private ChatView chatView;
    private BottomBarView bottomBarView;

    /**
     * Crea la vista del medio.
     * Esta muestra informacion del canal, los mensajes, y la barra para mandar mensajes
     */
    public ContentView(){

        this.setLayout(new BorderLayout());

        topBarView = new TopBarView();
        chatView = new ChatView();
        chatView.setName("CHATVIEW");
        bottomBarView = new BottomBarView();

        this.add(topBarView, BorderLayout.NORTH);
        this.add(chatView, BorderLayout.CENTER);
        this.add(bottomBarView, BorderLayout.SOUTH);

    }

    /**
     * Registra los controladores a la vista
     * @param actionListener listener para los botones
     * @param mouseListener listener para mostrar la hora de los mensajes al pasar el raton por encima
     */
    public void registerController(ActionListener actionListener, MouseListener mouseListener, KeyListener keyListener, AdjustmentListener adjustmentListener, FocusListener focusListener){

        topBarView.registerController(actionListener, mouseListener);
        chatView.registerController(mouseListener, adjustmentListener, actionListener);
        bottomBarView.registerController(actionListener, keyListener, focusListener);
    }

    /**
     * Añade un nuevo bloque de mensajes empezando con el mensaje indicado en
     * esta funcion.
     * @param msg texto del mensaje a mostrar
     * @param username nombre del usuario creador del mensaje
     * @param image path de la imagen del usuario
     * @param date fecha del mensaje
     * @param color color del fondo del bloque de mensajes
     */
    public void addFirstMessage(String msg, String username, ImageIcon image, Date date, Color color,boolean isMine) {
        // Color verde new Color(204, 255, 204);
        chatView.addFirstMessage(msg, username, image, date, color,isMine);
    }

    /**
     * Añade un mensaje al ultimo bloque de mensajes correspondiente al ultimo
     * addFirstMessage() hecho.
     * @param msg texto del mensaje a mostrar
     * @param date fecha del mensaje
     */
    public void addMessage(String msg, Date date, boolean isMine) {
        chatView.addMessage(msg, date, isMine);
    }

    /**
     * Añade una imagen al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     */
    public void addImageToMessage(Date date, BufferedImage image, int idMessage) {
        chatView.addImageToMessage(date, image, idMessage);
    }

    /**
     * Añade un placeholder del file al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     * @param filename nombre del fichero
     */
    public void addFileToMessage(Date date, BufferedImage image, String filename, int idMessage) {
        chatView.addFileToMessage(date, image, filename, idMessage);
    }

    /**
     * Añade una notificacion de un nuevo dia en el chat
     * @param day fecha del dia en formato String a mostrar
     */
    public void addNewDayNotification(String day) {
        chatView.addNewDayNotification(day);
    }

    /**
     * Añade un boton para pedir mas mensajes
     */
    public void addRequestMoreMessages(){
        chatView.addRequestMoreMessages();
    }

    /**
     * Devuelve una vista de tipo ChatView
     * @return ChatView
     */
    public ChatView getChatView() {
        return chatView;
    }

    /**
     * Devuelve el mensaje escrito actualmente
     * @return el mensaje que escribe el usuario
     */
    public String getMessage(){
        return bottomBarView.getMessageField();
    }

    /**
     * Borra el mensaje que escribe el usuario
     */
    public void eraseMessage(){
        bottomBarView.eraseMessageField();
    }

    /**
     * Cambia el titulo del canal
     * @param title nuevo titulo del canal
     */
    public void setChannelTitle(String title){
        topBarView.setChannelName(title);
    }

    /**
     * Cambia el numero de usuarios del canal
     * @param num numero de usuarios del canal
     */
    public void setNumberOfUsers(String num){
        topBarView.setNumUsers(num);
    }

    /**
     * Cambia la descripcion del canal
     * @param description descripcion del canal
     */
    public void setDescription(String description){
        topBarView.setDescription(description);
    }

    /**
     * Prepara el chat para insertar una tanda de mensajes
     */
    public void clearChatView() {
        chatView.removeAll();
        chatView.clearChatView();
        chatView.revalidate();
        chatView.repaint();
        revalidate();
        repaint();
    }

    /**
     * Añade una notificacion en el chat de que ha cambiado el dia
     * @param msgNumber : numero de mensajes sin leer por debajo de la notificacion
     */
    public void addNotViewedNotification(int msgNumber) {
        chatView.addNotViewedNotification(msgNumber);
        revalidate();
        repaint();
    }

    /**
     * Activa o desactiva el boton de cargar mas mensajes
     * @param bool
     */
    public void setEnableRequestMoreMessages(boolean bool){ chatView.setEnableRequestMoreMessages(bool); }


    public void clearNotViewedNotification() {
        chatView.clearNotViewedNotification();
    }

    /**
     * Retorna la vista de la barra de abajo
     * @return la vista de la barra de abajo
     */
    public BottomBarView getBottomBarView() {
        return bottomBarView;
    }

    /**
     * Scrollea hacia abajo el chat
     */
    public void scrollToBottom() {
        chatView.scrollToBottom();
    }

    /**
     * Dice si el scroll del chat esta abajo o no
     *
     * @return booleano
     */
    public boolean isScrollOnBottom() {
        return chatView.isScrollOnBottom();
    }

    /**
     * Prepara el chat view para cargar una tanda de mensajes
     */
    public void prepareChatView() {
        chatView.prepareChatView();
    }
    /**
     * Incrementa el numero de notificaciones no vistas
     *
     * @return si se pudo incrementar o no
     */
    public boolean incrementNotViewedNotification() {
        return chatView.incrementNotViewedNotification();
    }
}
