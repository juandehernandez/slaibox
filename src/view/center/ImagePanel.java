package view.center;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Panel que contiene las imagenes que se muestran en el chat
 *
 * @author Sergi Valbuena
 * @version 03/05/2017
 */
public class ImagePanel extends JPanel {
    private Image image;
    private Date date;
    private JPopupMenu popupMenu;
    private int idMessage;

    /**
     * Constructor de la clase
     * @param image imagen
     * @param d dimension de la imagen
     * @param date fecha
     * @param idMessage id del mensaje
     */
    public ImagePanel(Image image, Dimension d, Date date, int idMessage) {
        this.image = image;
        setOpaque(false);
        int width = (int) d.getWidth();
        int heigth = (int) d.getHeight();
        d = new Dimension(width, heigth + 10);
        setPreferredSize(d);
        this.image = image.getScaledInstance(width, heigth, Image.SCALE_SMOOTH);
        this.date = date;
        popupMenu = new JPopupMenu();

        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String hour = "Sent at " + formatter.format(date.getTime());
        popupMenu.add(new JMenuItem(hour));

        this.idMessage = idMessage;

    }

    /**
     * Pinta el componente
     * @param g el grafico
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image,0,0, this);
    }


    /**
     * Muestra el pop up con la fecha en la que se escribio el mensaje
     * @param location
     */
    public void showTimePopUp(Point location) {
        popupMenu.show(this, location.x + 10, location.y + 5);
    }

    /**
     * Esconde el pop up con la fecha en la que se escribio el mensaje
     */
    public void hideTimePopUp() {
        popupMenu.setVisible(false);
    }

    /**
     * devuelve el id del Mensaje
     * @return id del mensaje
     */
    public int getIdMessage(){
        return idMessage;
    }
}
