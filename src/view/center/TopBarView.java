package view.center;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * * Vista inferior del Chat del cliente
 * @author Matias
 * @version 1.1.0
 */
public class TopBarView extends JPanel {

    /* Color Constant */
    private static final Color COLOR_SUPERIOR_LIGHT = new Color(130,100,130);
    private static final Color COLOR_DARK = new Color(100,70,100);

    private final JLabel jlChannelName;
    private final JLabel jlUsers;
    private final JLabel jlDesciption;
    private final JButton jbFiles;
    private final JButton jbChannelDetails;

    /**
     * Crea la vista de la barra superior
     */
    public TopBarView(){

        /* dejamos el Panel como Border */
        this.setLayout(new BorderLayout());
        this.setBackground(COLOR_SUPERIOR_LIGHT);

        /* creamos el lado izquierdo de La Top Bar*/
        JPanel jpInfoContainer = new JPanel(new GridLayout (3,1, 0,1));
        jpInfoContainer.setBorder(new EmptyBorder(10,30,10,10));
        jpInfoContainer.setBackground(COLOR_SUPERIOR_LIGHT);

        jlChannelName = new JLabel("NomCanal");
        jlChannelName.setForeground(Color.white);
        jlUsers = new JLabel("Number of users: ..");
        jlUsers.setForeground(Color.white);
        jlDesciption = new JLabel("Brief description....");
        jlDesciption.setForeground(Color.white);
        jpInfoContainer.add(jlChannelName);
        jpInfoContainer.add(jlUsers);
        jpInfoContainer.add(jlDesciption);

        /* creamos el lado derecho del panel*/
        GridLayout gl = new GridLayout (1,2);
        gl.setHgap(5);
        JPanel jpButtonContainer = new JPanel(gl);
        jpButtonContainer.setBorder(new EmptyBorder(20,30,10,10));
        jpButtonContainer.setBackground(COLOR_SUPERIOR_LIGHT);

        jbFiles = new JButton("Files");
        jbFiles.setBackground(COLOR_DARK);
        jbFiles.setForeground(Color.WHITE);
        jbFiles.setOpaque(true);
        jbFiles.setBorderPainted(false);

        /* añadimos un icono al boton de archivos*/
        try {
            ImageIcon img = new ImageIcon(
                    ((BufferedImage)ImageIO.read(new File("resources/file_icon.png"))).getScaledInstance(20, 20,
                    Image.SCALE_SMOOTH));
            jbFiles.setIcon(img);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        jbChannelDetails = new JButton("Details");

        jbChannelDetails.setBackground(COLOR_DARK);
        jbChannelDetails.setForeground(Color.WHITE);
        jbChannelDetails.setOpaque(true);
        jbChannelDetails.setBorderPainted(false);

        /*añadimos un icono al boton de descripcion*/
        try {
            ImageIcon img = new ImageIcon(
                    ((BufferedImage)ImageIO.read(new File("resources/options_icon.png"))).getScaledInstance(20, 20,
                            Image.SCALE_SMOOTH));
            jbChannelDetails.setIcon(img);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        /*añadimos los botones al contenedor*/
        jpButtonContainer.add(jbChannelDetails);
        jpButtonContainer.add(jbFiles);

        /*añadimos los dos lados al Top Bar*/
        this.add(jpInfoContainer, BorderLayout.CENTER);
        this.add(jpButtonContainer, BorderLayout.LINE_END);

    }

    /**
     * Cambiar el nombre del canal
     * @param name el nombre a mostrar
     */
    public void setChannelName(String name){
        jlChannelName.setText(name);
    }

    /**
     * Cambiar la descripción del canal
     * @param description el mensaje a mostrar
     */
    public void setDescription(String description){
        jlDesciption.setText(description);
    }

    /**
     * Cambiar el numero de usuarios pertenencientes al canal
     * @param number el numero de usuarios en el canal
     */
    public void setNumUsers(String number){
        jlUsers.setText("Number of users: " + number);
    }

    /**
     * Registra un ActionListener a los botones
     * @param actionListener
     */
    public void registerController(ActionListener actionListener, MouseListener mouseListener){

        jbFiles.setActionCommand("FILES");
        jbFiles.addActionListener(actionListener);

        jbChannelDetails.setActionCommand("CHANNEL_DETAILS");
        jbChannelDetails.addActionListener(actionListener);

        jlUsers.addMouseListener(mouseListener);

    }

}
