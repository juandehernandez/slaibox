package view.left;

import view.custom_ui.CustomScrollBarUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe encargada de generar la view de toda la barra lateral izquierda
 *
 * @author : David
 */
public class MenuBarView extends JPanel {

    /* Color Constant */
    private static final Color COLOR_LATERAL = new Color(100,70,100);
    /* String Constant */
    public static final String CANALS = "Canals";
    public static final String CHATS = "Chats";
    public static final String PROFILE = "Profile";
    public static final String DELETE = "Delete";

    /* JTextField */
    private final JTextField jtfBuscador = new JTextField();
    /* JLabel */
    private final JLabel jlImagenUsuario = new JLabel();
    private final JLabel jlLogin = new JLabel("Loading...");
    private final JLabel jlDisponible = new JLabel("Online");
    private JButton jbCross;

    /* JPanel */
    private final JPanel jpPerfil;

    private JPChats jpcCanales;
    private JPChats jpcChats;

    /**
     * Genera todos los componentes de la barra lateral
     *
     * @Constructor
     */
    public MenuBarView(){
        /* Obtenemos la Screen Height para poder escoger el numero de canales
         * a mostrar en funcion del tamaño de la pantalla del usuario */
        int screenHeight = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        /* Creamos el JPChat (extends JPanel) que contendra los canales */
        jpcCanales = new JPChats(screenHeight/(4*18),CANALS);
        jpcCanales.setBackground(COLOR_LATERAL);
        jpcCanales.setBorder(new EmptyBorder(5,0,0,0));
        /* Creamos el JPChat (extends JPanel) que contendra los chats */
        jpcChats = new JPChats(screenHeight/(4*18),CHATS);
        jpcChats.setBackground(COLOR_LATERAL);
        jpcChats.setBorder(new EmptyBorder(5,0,0,0));
        /* Set Box Layouts */
        BoxLayout blBarraLateral = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        this.setLayout(blBarraLateral);
        this.setBorder(new EmptyBorder(20,20,20,20));
        /* Añadimos la constante de color */
        this.setBackground(COLOR_LATERAL);
        this.setPreferredSize(new Dimension(280,0));

        /* Creamos el JPanel del Perfil */
        jpPerfil = creaPerfil();
        /* Creamos el JPanel del Buscador */
        final JPanel jpBuscador = creaBuscador();
        /* Creamos el JPanel de los canales */
        final JPanel jpCanales = creaCanales();
        /* Creamos el JPanel de los chats */
        final JPanel jpChats = creaChats();

        /* Añadimos todo */
        this.add(jpPerfil);
        this.add(jpBuscador);
        this.add(jpCanales);
        this.add(jpChats);

    }

    /**
     * Crea el panel del buscador
     * @return devuelve un JPanel del buscador
     */
    private JPanel creaBuscador(){
        /* Personalizamos el Panel del buscador */
        final JPanel jpBuscador = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jpBuscador.setPreferredSize(new Dimension(240,30));
        jpBuscador.setMaximumSize(new Dimension(240,30));
        jpBuscador.setBackground(Color.WHITE);
        /* Asignamos preferencias al TextView */
        jtfBuscador.setBorder(BorderFactory.createEmptyBorder());
        jtfBuscador.setPreferredSize(new Dimension(180,20));
        jtfBuscador.setMaximumSize(new Dimension(180,20));
        /* Añadimos la imagen de la Lupa al lado del TextField */
        JLabel labelLupa;
        try{
            labelLupa = new JLabel( new ImageIcon((ImageIO.read(
                    new File("resources/lupa.png")))
                    .getScaledInstance(20, 20,Image.SCALE_SMOOTH)));
        }catch (IOException e){
            e.printStackTrace();
            labelLupa = new JLabel();
        }
        /* Fondo Blanco a la imagen */
        labelLupa.setBackground(Color.WHITE);
        labelLupa.setPreferredSize(new Dimension(20,20));
        labelLupa.setMaximumSize(new Dimension(20,20));
        /* Añadimos los componentes al panel General */
        jbCross = new JButton();
        try{
            jbCross.setIcon( new ImageIcon(( ImageIO.read(
                    new File("resources/cross.png"))).getScaledInstance(20, 20,Image.SCALE_SMOOTH)));
        }catch (IOException e){
            e.printStackTrace();
        }
        jbCross.setName(DELETE);
        jbCross.setPreferredSize(new Dimension(20,20));
        jbCross.setMaximumSize(new Dimension(20,20));
        jbCross.setBackground(Color.WHITE);
        jbCross.setOpaque(true);
        jpBuscador.add(jtfBuscador);
        jpBuscador.add(jbCross);
        jpBuscador.add(labelLupa);
        return jpBuscador;
    }

    /**
     * Crea el panel del perfil con la imagen del perfil, el nombre de usuario i
     * @return Devuelve un JPanel con todo el contenido
     */
    private JPanel creaPerfil(){
        /* Creamos el apartado del perfil */
        final JPanel jpPerfil = new JPanel(new GridLayout(1,2));
        jpPerfil.setName(PROFILE);
        jpPerfil.setBorder(new EmptyBorder(5,0,10,0));
        jpPerfil.setPreferredSize(new Dimension(170,105));
        jpPerfil.setMaximumSize(new Dimension(190,105));
        jpPerfil.setBackground(COLOR_LATERAL);
        /* Asignem Avatar d'usuari */
        asignaImagen("resources/avatar.png");
        /* Creamos apartado de login */
        final JPanel jpLogin = new JPanel(new GridLayout(2,1));
        jpLogin.setBackground(COLOR_LATERAL);
        jpLogin.setBorder(new EmptyBorder(5,15,5,15));
        jlLogin.setForeground(Color.WHITE);
        jlDisponible.setForeground(Color.WHITE);
        jlDisponible.setFont(new Font(jlDisponible.getFont().getName(), Font.PLAIN, 12));
        jpLogin.add(jlLogin);
        jpLogin.add(jlDisponible);
        JPanel jpAux = new JPanel();
        jpAux.setBackground(Color.WHITE);
        jpAux.add(jlImagenUsuario);
        /* Añadimos todo al layout del Perfil */
        jpPerfil.add(jpAux);
        jpPerfil.add(jpLogin);
        return jpPerfil;
    }

    /**
     * Metodo que asigna una imagen al Label de la imagen del perfil
     * @param path String que contiene la ruta a la imagen
     */
    public void asignaImagen(String path){
        try {
            jlImagenUsuario.setIcon((new ImageIcon(
                    ((BufferedImage)ImageIO.read(new File(path))).getScaledInstance(80, 80,
                            Image.SCALE_SMOOTH))));
        } catch (Exception e) {
            System.out.println("Could not load the image.");
        }
    }

    /**
     * Metodo que asigna una imagen al Label de la imagen del perfil
     * @param img imagen a añadir
     */
    public void asignaImagen(Image img) {
        try {
            jlImagenUsuario.setIcon((new ImageIcon(img.getScaledInstance(80, 80, Image.SCALE_SMOOTH))));
        } catch (Exception e) {
            System.out.println("Could not load the image.");
        }
    }

    /**
     * Metodo que crea el panel de la lista de los canales publicos
     * @return el JPanel rellenado
     */
    private JPanel creaCanales(){
        /* Creamos el Panel general y lo personalizamos */
        final JPanel jpCanales = new JPanel();
        jpcCanales.setName(CANALS);
        jpCanales.setMinimumSize(new Dimension(100,100));
        jpCanales.setBorder(new EmptyBorder(10,0,0,0));
        jpCanales.setBackground(COLOR_LATERAL);
        BoxLayout boxLayout = new BoxLayout(jpCanales,BoxLayout.PAGE_AXIS);
        jpCanales.setLayout(boxLayout);
        /* Creamos un panel para contener nuestro JLabel del titulo del apartado
         * para alinearlo a la izquierda */
        final JPanel jAlignText = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jAlignText.setBackground(COLOR_LATERAL);
        /* Creamos el titulo del apartado y lo personalizamos */
        final JLabel canales = new JLabel("Canales: ");
        canales.setFont(new Font(canales.getFont().getName(), Font.BOLD, 19));
        canales.setForeground(Color.WHITE);
        /* Creamos el panel que permite ocupar a los canales el espacio necesario */
        final JPanel centerPanel = new JPanel(new BorderLayout());
        centerPanel.setBackground(COLOR_LATERAL);
        final JScrollPane jspCanals = new JScrollPane(jpcCanales);
        jspCanals.setBackground(COLOR_LATERAL);
        jspCanals.setBorder(null);
        /* Customizamos la ScrollBar */
        jspCanals.getVerticalScrollBar().setPreferredSize(new Dimension(3,0));
        jspCanals.getVerticalScrollBar().setUI(new CustomScrollBarUI(Color.WHITE,new Color(90,60,90)));
        jspCanals.getVerticalScrollBar().setBackground(COLOR_LATERAL);
        jspCanals.getVerticalScrollBar().setBorder(null);
        jspCanals.getVerticalScrollBar().setUnitIncrement(16);
        jspCanals.getHorizontalScrollBar().setPreferredSize(new Dimension(3,0));
        jspCanals.getHorizontalScrollBar().setUI(new CustomScrollBarUI(Color.WHITE,new Color(90,60,90)));
        jspCanals.getHorizontalScrollBar().setBackground(COLOR_LATERAL);
        jspCanals.getHorizontalScrollBar().setBorder(null);
        jspCanals.getHorizontalScrollBar().setUnitIncrement(16);
        /* Añadimos cada componente a su correspondiente Panel */
        centerPanel.add(jspCanals,BorderLayout.CENTER);
        jAlignText.add(canales);
        jpCanales.add(jAlignText);
        jpCanales.add(centerPanel);

        return jpCanales;
    }

    /**
     * Metodo que crea el panel de la lista de los canales privados
     * @return el JPanel rellenado
     */
    private JPanel creaChats(){
        /* Creamos el Panel general y lo personalizamos */
        final JPanel jpChats = new JPanel();
        jpcChats.setName(CHATS);
        jpChats.setMinimumSize(new Dimension(100,100));
        jpChats.setBorder(new EmptyBorder(5,0,5,0));
        jpChats.setBackground(COLOR_LATERAL);
        BoxLayout boxLayout = new BoxLayout(jpChats,BoxLayout.PAGE_AXIS);
        jpChats.setLayout(boxLayout);
        /* Creamos un panel para contener nuestro JLabel del titulo del apartado
         * para alinearlo a la izquierda */
        final JPanel jAlignText = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jAlignText.setBackground(COLOR_LATERAL);
        /* Creamos el titulo del apartado y lo personalizamos */
        final JLabel chats = new JLabel("Chats: ");
        chats.setForeground(Color.WHITE);
        chats.setFont(new Font(chats.getFont().getName(), Font.BOLD, 19));
        /* Creamos el panel que permite ocupar a los canales el espacio necesario */
        final JPanel centerPanel = new JPanel(new BorderLayout());
        centerPanel.setBackground(COLOR_LATERAL);
        final JScrollPane jspChats = new JScrollPane(jpcChats);
        jspChats.setBackground(COLOR_LATERAL);
        jspChats.setBorder(null);
        /* Customizamos la ScrollBar */
        jspChats.getVerticalScrollBar().setPreferredSize(new Dimension(3,0));
        jspChats.getVerticalScrollBar().setUI(new CustomScrollBarUI(Color.WHITE,new Color(90,60,90)));
        jspChats.getVerticalScrollBar().setBackground(COLOR_LATERAL);
        jspChats.getVerticalScrollBar().setBorder(null);
        jspChats.getVerticalScrollBar().setUnitIncrement(16);
        jspChats.getHorizontalScrollBar().setPreferredSize(new Dimension(3,0));
        jspChats.getHorizontalScrollBar().setUI(new CustomScrollBarUI(Color.WHITE,new Color(90,60,90)));
        jspChats.getHorizontalScrollBar().setBackground(COLOR_LATERAL);
        jspChats.getHorizontalScrollBar().setBorder(null);
        jspChats.getHorizontalScrollBar().setUnitIncrement(16);
        /* Añadimos cada componente a su correspondiente Panel */
        centerPanel.add(jspChats);
        jAlignText.add(chats);
        jpChats.add(jAlignText);
        jpChats.add(centerPanel);

        return jpChats;
    }

    /**
     * Añade los canales publicos a su lista
     * @param canals Lista con los nombres de los canales
     * @param ids Lista con los ides de los canales
     */
    public void addCanals(java.util.List<String> canals,List<Integer> ids){
        jpcCanales.afegeixCanals(canals,ids);
        jpcCanales.registraControlador();
    }

    /**
     * Añade los canales privados a su lista
     * @param chats Lista con los nombres de los canales
     * @param ids Lista con los ides de los canales
     */
    public void addChats(java.util.List<String> chats,List<Integer> ids){
        jpcChats.afegeixCanals(chats,ids);
        jpcChats.registraControlador();
    }

    /**
     * Añade los controladores correspondientes a los componentes de la barra lateral
     * @param mouseListener Mouse listener para funciones del mouse
     * @param documentListener Document Listener para funciones del buscador
     */
    public void registrarControladorBarraLateral(MouseListener mouseListener, DocumentListener documentListener){
        jpPerfil.addMouseListener(mouseListener);
        jpcCanales.setController(mouseListener);
        jpcChats.setController(mouseListener);
        jpcCanales.registraControlador();
        jpcChats.registraControlador();
        jtfBuscador.getDocument().addDocumentListener(documentListener);
        jbCross.addMouseListener(mouseListener);
    }

    /**
     * Metodo que muestra todos los canales de las listas
     */
    public void mostraTotsCanals(){
        jpcChats.mostraTotsCanals();
        jpcCanales.mostraTotsCanals();
    }

    /**
     * Metodo que esconde los canales recibidos de los canales de las listas
     * @param canalesEscondidos lista de canales a esconder
     */
    public void escondeCanales(ArrayList<Integer> canalesEscondidos){
        jpcCanales.amagaCanalsIXats(canalesEscondidos);
        jpcChats.amagaCanalsIXats(canalesEscondidos);
    }

    /**
     * Establece los valores para el panel de perfil
     * @param username nombre del usuario
     * @param status Estado del usuario
     * @param profileImage Imagen del perfil
     */
    public void setProfile(String username,String status,ImageIcon profileImage){
        jlLogin.setText(username);
        jlDisponible.setText(status);
        try {
            Image image = profileImage.getImage();
            jlImagenUsuario.setIcon((new ImageIcon(image.getScaledInstance(70, 70,Image.SCALE_SMOOTH))));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not load the image.");
        }
    }

    /**
     * Actualiza el estado del perfil
     * @param estado String del estado
     */
    public void setEstado(String estado) {
        jlDisponible.setText(estado);
    }

    /**
     * Borra el texto del buscador
     */
    public void deleteText(){
        jtfBuscador.setText("");
    }

    /**
     * Borra el canal de la lista de canales
     * @param idChannel id del canal a borrar
     */
    public void deleteChannel(int idChannel){
        jpcChats.deletePrivateChannel(idChannel);
        jpcCanales.deletePrivateChannel(idChannel);
    }

    /**
     * Incrementa el numero de mensajes no vistos
     * @param channel canal al que incrementar
     */
    public void incrementaUnseen(int channel){
        jpcChats.incrementUnseenMessages(channel);
        jpcCanales.incrementUnseenMessages(channel);
    }

    /**
     * Establece un valor de mensajes sin leer
     * @param channel Canal al que establecer
     * @param number numero de mensajes sin leer
     */
    public void setUnseenMessage(int channel, int number){
        jpcCanales.setUnseenMessage(channel, number);
        jpcChats.setUnseenMessage(channel, number);
        jpcChats.repaint();
        jpcCanales.revalidate();
    }

    /**
     * Establece todos los mensajes como no vistos
     * @param channel canal a establecer los canales como vistos
     */
    public void setSeenMessages(int channel){
        jpcCanales.setSeenMessages(channel);
        jpcChats.setSeenMessages(channel);
    }

    /**
     * Añade 1 canal a la lista de canales
     * @param channel Nombre del canal
     * @param isPrivate privacidad del canal
     * @param id identificador del canal
     */
    public void addChannel(String channel,  boolean isPrivate,int id){
        if(!isPrivate) {
            jpcCanales.addChannel(channel,isPrivate,id);
        }else{
            jpcChats.addChannel(channel,isPrivate,id);
        }
    }

    /**
     * Actualiza el estatus de los chats privados
     * @param id identificador del canal
     * @param status String del status a cambiar
     */
    public void changeConnection(int id,String status){
        jpcChats.changeConnection(id,status);
    }

    /**
     * Establece un canal como seleccionado
     * @param idChannel identificador del canal
     */
    public void setSelectedChannel(int idChannel){
        jpcChats.setSelectedChannel(idChannel);
    }

    /**
     * Establece el ultimo canal seleccionado como  sin seleccionado
     */
    public void unselectChannel(){
        jpcChats.unselectChannel();
    }
}