package view.left;

import view.custom_ui.CustomJPanelNotifyUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Jpanel que contiene la lista de canales y un thread para actualizar los canales visibles
 *
 * @author : David
 */
public class JPChats extends JPanel implements Runnable {
    /* Lists */
    /* Lists de strings */
    private List<String> canales;
    /* Lists de Label */
    private List<JLabel> etiquetas;
    private List<JLabel> unseenArray;
    private List<JLabel> connectedLabel;
    /* Lists de Panel */
    private List<JPanel> panelsNotify;
    private List<JPanel> jpChannel;
    /* Lists de Integer */
    private List<Integer> ides;
    private List<Integer> hideChannels;
    /* Lists de Buttons */
    private List<JButton> buttons;

    private String tipusComunicacio;
    private JLabel lastPainted;
    private MouseListener mouseListener;
    private boolean hide;
    private boolean done;

    /* Constants */
    /* String */
    private static final String CONNECTED = "Connected";
    private static final String OCCUPIED = "Occupied";
    private static final String DISCONNECTED = "Disconnected";
    /* Color */
    private static final Color SELECTED_CHANNEL = new Color(140,110,140);
    private static final Color COLOR_LATERAL = new Color(100,70,100);

    /**
     * Constructor de la clase
     * @param limit altura limite
     * @param tipusComunicacio Indica si es privado o publico
     */
    public JPChats (int limit,String tipusComunicacio){
        this.setMaximumSize(new Dimension(500,limit));
        this.tipusComunicacio = tipusComunicacio;
        canales = new ArrayList<>();
        etiquetas = new ArrayList<>();
        unseenArray = new ArrayList<>();
        panelsNotify = new ArrayList<>();
        connectedLabel = new ArrayList<>();
        jpChannel = new ArrayList<>();
        ides = new ArrayList<>();
        buttons = new ArrayList<>();
        lastPainted = null;

        hide = false;
        done = true;

        /* Establecemso el layout */
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        setLayout(boxLayout);
        /* Empezamos el thread de visibilidad de los canales */
        new Thread(this).start();
    }

    /**
     * Añade todos los canales a la lista lateral
     *
     * @param canals lista con los nombres de cada canal
     * @param idCanal lista con el id de cada canal
     */
    public void afegeixCanals(List<String> canals,List<Integer> idCanal){
        /* Guardamos los nombres i los id */
        canales.addAll(canals);
        ides.addAll(idCanal);

        int sizeCanals = canals.size();
        for (int i = 0; i< sizeCanals;i++) {
            JLabel jLabel;
            JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            /* Añadimos el canal, en caso de publico ponemos un '#' delante sino un '-' */
            if(tipusComunicacio.equals(MenuBarView.CANALS)) {
                /* Canal publico */
                jLabel = new JLabel(" # " + canals.get(i));
                jLabel.setName(" # " +Integer.toString(idCanal.get(i)));
            }else{
                /* Canal privado */
                jLabel = new JLabel(" - " + canals.get(i));
                jLabel.setName(" - " +Integer.toString(idCanal.get(i)));
                /* al ser privado añadimos al opcion de borrarlo de la vista */
                JButton deletebutton = new JButton();
                buttons.add(deletebutton);
                deletebutton.addMouseListener(mouseListener);
                deletebutton.setName(" X " + Integer.toString( idCanal.get(i)));
                /* Customizamos el boton */
                deletebutton.setBackground(Color.WHITE);
                deletebutton.setOpaque(true);
                deletebutton.setContentAreaFilled(true);
                deletebutton.setBorderPainted(false);
                try {
                    Image image = ImageIO.read(new File("resources/cross.png"));
                    Image icon = image.getScaledInstance(10, 10, Image.SCALE_SMOOTH);
                    deletebutton.setIcon(new ImageIcon(icon));
                    deletebutton.setSize(20, 20);
                    deletebutton.setPreferredSize(new Dimension(20, 20));
                    jPanel.add(deletebutton);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            jLabel.setBorder(new EmptyBorder(0,0,2,0));
            jLabel.setFont(new Font(jLabel.getFont().getName(), Font.PLAIN, 15));
            jLabel.setForeground(Color.WHITE);
            /* Añadimos el panel y label de mensajes no leidos */
            JLabel jlUnseen = new JLabel("0");
            jPanel.add(jLabel);
            JPanel jpAux = new JPanel();
            jpAux.add(jlUnseen);
            jpAux.setUI(new CustomJPanelNotifyUI());
            jpAux.setVisible(false);
            jPanel.add(jpAux);
            JLabel jlConnected = null;
            /* Si el canal es privado añadimos tambien un label de conexion  */
            if(tipusComunicacio.equals(MenuBarView.CHATS)){
                jlConnected = new JLabel(DISCONNECTED);
                jlConnected.setForeground(Color.RED);
                jPanel.add(jlConnected);
                connectedLabel.add(jlConnected);
            }
            jPanel.setBackground(COLOR_LATERAL);
            jPanel.setMaximumSize(new Dimension(500,25));
            add(jPanel);
            jpChannel.add(jPanel);
            etiquetas.add(jLabel);
            panelsNotify.add(jpAux);
            unseenArray.add(jlUnseen);
        }
    }

    /**
     * metodo que añade el controlador al panel como atributo
     * @param mouseListener
     */
    public void setController(MouseListener mouseListener){
        this.mouseListener = mouseListener;
    }

    /**
     * Metodo que añade el mouse listener a cada panel de canal
     */
    public void registraControlador(){
        for (int i = 0; i< etiquetas.size();i++){
            etiquetas.get(i).addMouseListener(mouseListener);
        }
    }

    /**
     * Metodo para establecer que canales se tienen que esconder
     * @param hideChannels
     */
    public void amagaCanalsIXats(ArrayList<Integer> hideChannels){
        this.hideChannels = hideChannels;
        hide = true;
        done = false;
    }

    /**
     * Metodo para volver a mostrar todos los canales
     */
    public void mostraTotsCanals(){
        hide = false;
        done = false;
    }

    /**
     * Incrementa en 1 el numero de mensajes no leidos del canal recibido
     * @param channel canal al que hay que incrementar
     */
    public void incrementUnseenMessages(int channel){
        int pos = Integer.MIN_VALUE;
        for(int i = 0;i<ides.size();i++){
            if(ides.get(i) == channel){
                pos = i;
            }
        }
        if(pos != Integer.MIN_VALUE) {
            panelsNotify.get(pos).setVisible(true);
            unseenArray.get(pos).setText(Integer.toString(
                    Integer.valueOf(unseenArray.get(pos).getText()) + 1));
        }
    }

    /**
     * Metodo para establecer el numero de mensajes
     * @param channel canal al que establecer los mensajes correspondientes
     * @param number numero de mensajes no leidos
     */
    public void setUnseenMessage(int channel, int number){
        String numberString = Integer.toString(number);
        int pos = Integer.MIN_VALUE;
        for(int i = 0;i<ides.size();i++){
            if(ides.get(i) == channel){
                pos = i;
            }
        }
        if(pos != Integer.MIN_VALUE) {
            panelsNotify.get(pos).setVisible(true);
            panelsNotify.get(pos).repaint();
            panelsNotify.get(pos).revalidate();

            unseenArray.get(pos).setText(numberString);
            unseenArray.get(pos).repaint();
            unseenArray.get(pos).revalidate();

        }
        revalidate();
        repaint();
    }

    /**
     * Esconde el panel de los mensajes vistos para avisar de que todos los mensajes se han visto
     * @param idChannel canal correspondiente
     */
    public void setSeenMessages(int idChannel){
        int pos = Integer.MIN_VALUE;
        for(int i = 0;i<ides.size();i++){
            if(ides.get(i) == idChannel){
                pos = i;
            }
        }
        if(pos != Integer.MIN_VALUE) {
            panelsNotify.get(pos).setVisible(false);
            unseenArray.get(pos).setText("0");
        }
    }

    /**
     * Metodo para añadir 1 canal a la vista
     * @param channel canal al que añadir
     * @param isPrivate boolean que indica la privacidad
     * @param id identificador del canal
     */
    public void addChannel(String channel, boolean isPrivate,int id){
        JLabel jLabel;
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        if(tipusComunicacio.equals(MenuBarView.CANALS)) {
            jLabel = new JLabel(" # " + channel);
            jLabel.setName(" # " +Integer.toString(id));
        }else{
            jLabel = new JLabel(" - " + channel);
            jLabel.setName(" - " +Integer.toString(id));
            JButton deletebutton = new JButton();
            buttons.add(deletebutton);
            deletebutton.addMouseListener(mouseListener);
            deletebutton.setName(" X " + Integer.toString(id));
            deletebutton.setBackground(Color.WHITE);
            deletebutton.setOpaque(true);
            deletebutton.setContentAreaFilled(true);
            deletebutton.setBorderPainted(false);
            try {
                Image image = ImageIO.read(new File("resources/cross.png"));
                Image icon = image.getScaledInstance(10, 10, Image.SCALE_SMOOTH);
                deletebutton.setIcon(new ImageIcon(icon));
                deletebutton.setSize(20, 20);
                deletebutton.setPreferredSize(new Dimension(20, 20));
                jPanel.add(deletebutton);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        jLabel.setBorder(new EmptyBorder(0,0,2,0));
        jLabel.setFont(new Font(jLabel.getFont().getName(), Font.PLAIN, 15));
        jLabel.setForeground(Color.WHITE);
        jLabel.addMouseListener(mouseListener);
        JLabel jlUnseen = new JLabel("0");
        //jlUnseen.setBackground(SELECTED_CHANNEL);
        jPanel.add(jLabel);
        JPanel jpAux = new JPanel();
        jpAux.add(jlUnseen);
        jpAux.setUI(new CustomJPanelNotifyUI());
        jpAux.setVisible(false);
        jPanel.add(jpAux);
        JLabel jlConnected = null;
        if(tipusComunicacio.equals(MenuBarView.CHATS)){
            jlConnected = new JLabel(DISCONNECTED);
            jlConnected.setForeground(Color.RED);
            jPanel.add(jlConnected);
            connectedLabel.add(jlConnected);
        }
        jPanel.setBackground(COLOR_LATERAL);
        jPanel.setMaximumSize(new Dimension(500,25));
        add(jPanel);
        jpChannel.add(jPanel);
        etiquetas.add(jLabel);
        panelsNotify.add(jpAux);
        unseenArray.add(jlUnseen);
        ides.add(id);
        canales.add(channel);
        this.repaint();
        this.revalidate();
    }

    /**
     * Metodo para borrar un canal de la vista
     * @param channel id del canal que hay que borrar
     */
    public void deleteChannel(int channel){
        int pos = Integer.MIN_VALUE;
        for(int i = 0;i<ides.size();i++){
            if(ides.get(i) == channel){
                pos = i;
                break;
            }
        }
        if(pos != Integer.MIN_VALUE){
            canales.remove(pos);
            etiquetas.get(pos).setVisible(false);
            panelsNotify.get(pos).setVisible(false);
            unseenArray.get(pos).setVisible(false);
            jpChannel.get(pos).setVisible(false);
            if(tipusComunicacio.equals(MenuBarView.CHATS)){
                connectedLabel.get(pos).setVisible(false);
                buttons.get(pos).setVisible(false);
                connectedLabel.remove(pos);
                buttons.remove(pos);
            }
            jpChannel.remove(pos);
            etiquetas.remove(pos);
            panelsNotify.remove(pos);
            unseenArray.remove(pos);
            ides.remove(pos);
            this.repaint();
            this.revalidate();
        }
    }

    /**
     * Thread que se encarga de actualizar la visibilidad de los canales
     */
    @Override
    public void run() {
        boolean found = false;
        while(true){
            if(hide){
                if (!done) {
                    found = false;
                    for (int i = 0; i < etiquetas.size(); i++) {
                        for (int j = 0; j < hideChannels.size(); j++) {
                            if (ides.get(i) == hideChannels.get(j)) {
                                found = true;
                            }
                        }
                        if (found) {
                            etiquetas.get(i).setVisible(false);
                            jpChannel.get(i).setVisible(false);
                            if(tipusComunicacio.equals(MenuBarView.CHATS)){
                                buttons.get(i).setVisible(false);
                                connectedLabel.get(i).setVisible(false);
                            }
                        } else {
                            etiquetas.get(i).setVisible(true);
                            jpChannel.get(i).setVisible(true);
                            if(tipusComunicacio.equals(MenuBarView.CHATS)){
                                buttons.get(i).setVisible(true);
                                connectedLabel.get(i).setVisible(true);
                            }
                        }
                        found = false;
                    }
                    done = true;
                }
            }else{
                if(!done) {
                    for (int i = 0; i < etiquetas.size(); i++) {
                        etiquetas.get(i).setVisible(true);
                        jpChannel.get(i).setVisible(true);
                        if(tipusComunicacio.equals(MenuBarView.CHATS)){
                            buttons.get(i).setVisible(true);
                            connectedLabel.get(i).setVisible(true);
                        }
                    }
                    done = true;
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo para borrar un canal privado
     * @param idChannel identificador del canal a borrar
     */
    public void deletePrivateChannel(int idChannel){
        int pos = Integer.MIN_VALUE;
        for(int i = 0;i<ides.size();i++){
            if(ides.get(i) == idChannel){
                pos = i;
                break;
            }
        }
        if(pos != Integer.MIN_VALUE){
            /* Como no queremos borrar la información solo lo ponemos como no visible */
            etiquetas.get(pos).setVisible(false);
            jpChannel.get(pos).setVisible(false);
            if(tipusComunicacio.equals(MenuBarView.CHATS)){
                connectedLabel.get(pos).setVisible(false);
                buttons.get(pos).setVisible(false);
            }
            this.repaint();
            this.revalidate();
        }
    }

    /**
     * Metodo que actualiza el Status de los usuarios en canales privados
     * @param idCanal identificador del canal
     * @param status String del estado a cambiar
     */
    public void changeConnection(int idCanal,String status){
        for(int i = 0; i< ides.size();i++){
            if(ides.get(i) == idCanal){
                connectedLabel.get(i).setText(status);
                if(status != null) {
                    if (status.equals(CONNECTED)) {
                        connectedLabel.get(i).setForeground(Color.GREEN);
                    }
                    if (status.equals(OCCUPIED)) {
                        connectedLabel.get(i).setForeground(Color.ORANGE);
                    }
                    if (status.equals(DISCONNECTED)) {
                        connectedLabel.get(i).setForeground(Color.RED);
                    }
                }
            }
        }
    }

    /**
     * Metodo para pintar el fondo de un canal par mostrar el canal actual
     * @param idCanal id  del canal
     */
    public void setSelectedChannel(int idCanal){
        for(int i = 0; i< ides.size();i++){
            if(ides.get(i) == idCanal){
                if(lastPainted != null){
                    lastPainted.setBackground(COLOR_LATERAL);
                }
                etiquetas.get(i).setBackground(SELECTED_CHANNEL);
                etiquetas.get(i).setOpaque(true);
                jpChannel.get(i).setVisible(true);
                if(!etiquetas.get(i).isVisible()) {
                    etiquetas.get(i).setVisible(true);
                    connectedLabel.get(i).setVisible(true);
                    buttons.get(i).setVisible(true);
                }
                this.repaint();
                this.revalidate();
                lastPainted = etiquetas.get(i);
            }
        }
    }

    /**
     * Metodo para desmarcar el ultimo canal seleccionado
     */
    public void unselectChannel(){
        if(lastPainted != null) {
            lastPainted.setBackground(COLOR_LATERAL);
        }
    }
}