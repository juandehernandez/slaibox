package view;

import view.center.ContentView;
import view.left.MenuBarView;
import view.right.InfoCanal;
import view.center.UserView;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Frame del cliente
 *
 * @author : David
 */
public class MainView extends JFrame {

    /* String constants */
    public static final String USER_INFO = "User Info";
    public static final String CONTENT_VIEW = "Content view";
    public static final String WELCOME = "Welcome";
    public static final String LOADING = "Loading";

    /* Color Constants */
    private static final Color COLOR_SUPERIOR = new Color(50,20,50);
    private static final Color COLOR_SUPERIOR_LIGHT = new Color(70,40,70);

    /* ContentView */
    private final ContentView contentView;

    /* Barra Lateral */
    private MenuBarView jpBarraLateral;

    /* Info Canal */
    private final InfoCanal infoCanal;

    /* CardLayout */
    private final CardLayout cardLayout = new CardLayout();

    /* JPanels */
    private final JPanel centralPanel = new JPanel();
    private UserView userView;


    /**
     * Constructor de la clase
     */
    public MainView(){
        setTitle("SlaiBox");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize()));
        //setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension((int) (screenSize.getWidth() * 0.7), (int) (screenSize.getHeight() * 0.7)));
        //setUndecorated(true);
        //setResizable(false);
        /* Centramos Vista */
        //setLocationByPlatform(true);
        setLocationRelativeTo(null);
        /* Añadimos Layout */
        setLayout(new BorderLayout());

        try {
            Image image = ImageIO.read(new File("resources/logo.png"));
            setIconImage(image);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Could not load the icon of the window.");
        }

        /* Añadimos la barra lateral */
        jpBarraLateral = new MenuBarView();
        userView = new UserView("Nom");
        this.add(jpBarraLateral,BorderLayout.LINE_START);

        contentView = new ContentView();

        infoCanal = new InfoCanal();
        JPanel jpWelcome = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Image image = null;
                try{
                    image = ImageIO.read(new File("resources/Welcome.png"));
                }catch (IOException e){
                    System.out.println("Could not load the image");
                }
                if (image != null) {
                    image = image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH);
                    setOpaque(true);
                    Point p = this.getLocation();
                    g.drawImage(image, p.x, p.y, this);
                }
            }
        };
        JPanel jpLoading = new JPanel(new GridBagLayout());
        jpLoading.setBackground(new Color(229,239,241));
        JLabel jlLoad = new JLabel();
        jlLoad.setIcon(new ImageIcon("resources/loading_small.gif"));
        jpLoading.add(jlLoad);

        centralPanel.setLayout(cardLayout);
        centralPanel.add(contentView,CONTENT_VIEW);
        centralPanel.add(userView,USER_INFO);
        centralPanel.add(jpWelcome,WELCOME);
        centralPanel.add(jpLoading,LOADING);

        add(centralPanel, BorderLayout.CENTER);

        add(infoCanal, BorderLayout.EAST);
        setInfoCanalVisible(false);
    }

    /**
     * Registra los constroladores de la vista
     * @param listener controlador
     */
    public void registerController(WindowListener listener){
        addWindowListener(listener);
    }

    /**
     * Registra los controladores del Content View
     * @param actionListener controla los botones de la vista
     * @param mouseListener  usa para mostrar mensaje cuando de pasa el raton por encima de paneles y detectar clicks
     * @param keyListener detecta el uso de la teclas
     * @param adjustmentListener detecta cambios en el scroll
     * @param focusListener detecta si la barra del mensaje tiene foco
     */
    public void registerContentViewControllers(ActionListener actionListener, MouseListener mouseListener, KeyListener keyListener, AdjustmentListener adjustmentListener, FocusListener focusListener) {
        contentView.registerController(actionListener, mouseListener, keyListener, adjustmentListener, focusListener);
    }

    /**
     * Registra los controladores del Lateral View
     * @param mouseListener detecta clicks en el indice de los canales
     * @param documentListener filtra los canales
     */
    public void registerLateralControllers(MouseListener mouseListener, DocumentListener documentListener){
        jpBarraLateral.registrarControladorBarraLateral(mouseListener,documentListener);
    }

    /**
     * Registra los controladores del User View
     * @param al logout y cambiar el estado del usuario
     */
    public void registerUserViewController(ActionListener al) {
        userView.registraControlador(al);
    }

    /**
     * Registra los controladores del Info Panel
     * @param actionListener botones de iniciar chat privado
     * @param mouseListener descargar imagenes de la vista
     */
    public void registerInfoPanelController(ActionListener actionListener, MouseListener mouseListener){
       infoCanal.registerController(actionListener,mouseListener);
    }

    /**
     * Devuelve el mensaje introducido por el usuario
     * @return
     */
    public String getMessage(){
        return contentView.getMessage();
    }

    /**
     *Borra el mensaje, dejando el campo en blanco
     */
    public void eraseMessage(){
        contentView.eraseMessage();
    }

    /**
     * Devuelve la informacion del canal
     * @return
     */
    public InfoCanal getInfoCanal(){
        return infoCanal;
    }

    /**
     * Carga la informacion del canal
     * @param channelName
     * @param description
     * @param users
     */
    public void loadChannelInfo(String channelName, String description, ArrayList<String> users){
        contentView.setChannelTitle(channelName);
        contentView.setDescription(description);
        contentView.setNumberOfUsers(String.valueOf(users.size()));
    }

    /**
     * Establece la visibilidad de la barra de la informacion del canal
     * @param b boolean de visibilidad
     */
    public void setInfoCanalVisible(boolean b) {
        infoCanal.setVisible(b);
    }

    /**
     * Elimina toda la informacion del canal actual de la vista
     */
    public void removeInfoCanal(){
        infoCanal.removeAllInfoRight();
    }

    /**
     * Muestra la informacion del perfil del usuario
     * @param username nombre del usuario
     * @param email correo del usuario
     * @param status estado del usuario
     * @param profileImage Imagen del usuario
     */
    public void mostraUserInfo(String username, String email,String status, ImageIcon profileImage){
        cardLayout.show(centralPanel,USER_INFO);
        userView.setUsername(username);
        userView.setEmail(email);
        userView.setStatus(status);
        userView.setProfileImage(profileImage);
    }

    /**
     * Cambia el card layout al content view
     */
    public void mostraContentView(){
        cardLayout.show(centralPanel,CONTENT_VIEW);
    }

    /**
     * Cambia el card layout al panel de Welcome
     */
    public void mostraWelcome(){
        cardLayout.show(centralPanel,WELCOME);
    }

    /**
     * Cambia el card layout a la ventana de loading
     */
    public void mostraLoading(){
        cardLayout.show(centralPanel,LOADING);
    }

    /**
     * @return Devuelve el panel de la barra lateral
     */
    public MenuBarView getJpBarraLateral(){
        return jpBarraLateral;
    }

    /**
     * Añade un nuevo bloque de mensajes empezando con el mensaje indicado en
     * esta funcion.
     * @param msg texto del mensaje a mostrar
     * @param username nombre del usuario creador del mensaje
     * @param image path de la imagen del usuario
     * @param date fecha del mensaje
     */
    public void addFirstMessage(String msg, String username, ImageIcon image, Date date,boolean isMine) {
        // Color verde new Color(204, 255, 204);
        if(isMine) {
            contentView.addFirstMessage(msg, username, image, date, new Color(170,255,255), isMine);
        }else{
            contentView.addFirstMessage(msg, username, image, date, new Color(180,255,180), isMine);
        }
    }

    /**
     * Añade un mensaje al ultimo bloque de mensajes correspondiente al ultimo
     * addFirstMessage() hecho.
     * @param msg texto del mensaje a mostrar
     * @param date fecha del mensaje
     */
    public void addMessage(String msg, Date date, boolean isMine) {
        contentView.addMessage(msg, date, isMine);
    }

    /**
     * Añade una imagen al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     */
    public void addImageToMessage(Date date, BufferedImage image, int idMessage) {
        contentView.addImageToMessage(date, image, idMessage);
    }

    /**
     * Añade un placeholder del file al ultimo bloque de mensajes (al ultimo AddFirstMessage() hecho)
     * @param date fecha de la imagen
     * @param image imagen a mostrar
     * @param filename nombre del fichero
     */
    public void addFileToMessage(Date date, BufferedImage image, String filename, int idMessage) {
        contentView.addFileToMessage(date, image, filename, idMessage);
    }

    /**
     * Añade una notificacion de un nuevo dia en el chat
     * @param day fecha del dia en formato String a mostrar
     */
    public void addNewDayNotification(String day) {
        contentView.addNewDayNotification(day);
    }

    /**
     * Añade un boton para pedir mas mensajes
     */
    public void addRequestMoreMessages(){
        contentView.addRequestMoreMessages();
    }

    /**
     * Activa o desactiva el boton de cargar mas mensajes
     * @param bool
     */
    public void setEnableRequestMoreMessages(boolean bool){ contentView.setEnableRequestMoreMessages(bool); }

    /**
     * Limpia la vista del contentView
     */
    public void clearChatView() {
        contentView.clearChatView();
        revalidate();
        validate();
    }

    /**
     * Añade una notificacion en el chat de que ha cambiado el dia
     * @param msgNumber : numero de mensajes sin leer por debajo de la notificacion
     */
    public void addNotViewedNotification(int msgNumber) {
        contentView.addNotViewedNotification(msgNumber);
    }

    /**
     * Elimina la notificacion del ultimo mensaje leido de la content view
     */
    public void clearNotViewedNotification() {
        contentView.clearNotViewedNotification();
    }

    /**
     * @return devuelve el panel del content view
     */
    public ContentView getContentView() {
        return contentView;
    }

    /**
     * @return devuelve el panel de la UserView
     */
    public UserView getUserView() {
        return userView;
    }

    /**
     * @return devuelve el panel de la barra lateral izquierda
     */
    public MenuBarView getMenuBarView() {
        return jpBarraLateral;
    }

    /**
     * scrollea la vista del content view hasta el final
     */
    public void scrollToBottom() {
        contentView.scrollToBottom();
    }

    /**
     * @return devuelve true si esta hecho el scroll hasta el final, false sino
     */
    public boolean isScrollOnBottom() {
        return contentView.isScrollOnBottom();
    }

    /**
     * Incrementa el numero de mensajes sin leer
     * @return devuelve true si se ha creado el panel falso sino.
     */
    public boolean incrementNotViewedNotification() {
        return contentView.incrementNotViewedNotification();
    }

    /**
     * Elimina un canal de la barra lateral
     * @param channel id del canal
     */
    public void deleteChannel(int channel){
        jpBarraLateral.deleteChannel(channel);
    }

    /**
     * Actualiza el numero de usuarios del canal
     * @param users Lista de usuarios
     */
    public void updateChannelInfo(ArrayList<String> users){
        contentView.setNumberOfUsers(String.valueOf(users.size()));
    }

    /**
     * Prepara el chat view para cargar una tanda de mensajes
     */
    public void prepareChatView() {
        contentView.prepareChatView();
    }
}
