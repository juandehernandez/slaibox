package view.custom_ui;

import javax.swing.*;
import javax.swing.plaf.PanelUI;
import java.awt.*;

/**
 * Custom panel para ultimo mensaje leido
 *
 * @author : David
 */
public class CustomJPanelNotifyUI extends PanelUI {

    private static final Color SELECTED_CHANNEL = new Color(140,110,140);

    /* Pintamos un Rectangulo redondo */
    @Override
    public void update(Graphics g, JComponent c){
        if (c.isOpaque()) {
            g.setColor(SELECTED_CHANNEL);
            g.fillRoundRect(0,0,c.getWidth(),c.getHeight(),45,45);
        }
        paint(g, c);
    }

}
