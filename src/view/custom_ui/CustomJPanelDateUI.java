package view.custom_ui;

import javax.swing.*;
import javax.swing.plaf.PanelUI;
import java.awt.*;

/**
 * Classe que personaliza el aspecto del JPanel
 *
 * @author : David
 */
public class CustomJPanelDateUI extends PanelUI {

    /* Pintamos un Rectangulo redondo */
    @Override
    public void update(Graphics g, JComponent c){
        if (c.isOpaque()) {
            g.setColor(c.getBackground());
            g.fillRoundRect(0, 0, c.getParent().getWidth(),c.getHeight(),20,20);
        }
        paint(g, c);
    }

}
