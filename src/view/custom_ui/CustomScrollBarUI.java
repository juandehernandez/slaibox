
package view.custom_ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 * Classe Utilizada para personalizar una barra de Scroll
 *
 * @author : David
 */
public class CustomScrollBarUI extends BasicScrollBarUI{

    /** Color de la barra*/
    private Color thumbColor;
    /** Color del fons */
    private Color trackColor;

    /** Boton auxiliar para quitar los botones para hacer scroll hacia arriba y abajo */
    private JButton b = new JButton() {

        @Override
        public Dimension getPreferredSize() {
            /* Boton "invisible" (Dimension 0,0) */
            return new Dimension(0, 0);
        }

    };

    /**
     * Contructor que pone el color del thumb y del track
     * @constructor
     */
    public CustomScrollBarUI(Color barra,Color fondo) {
        thumbColor = barra;
        trackColor = fondo;
    }

    @Override
    /* Pintamos el la barra de scroll como un rectangulo a partir de la posicion y el tamaño */
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        //Triem el color del "thumb".
        g.setColor(thumbColor);
        ((Graphics2D)g).fillRect(thumbBounds.x, thumbBounds.y, thumbBounds.width, thumbBounds.height);
    }

    @Override
    /* Pintamos el la barra del fondo como un rectangulo a partir de la posicion y el tamaño */
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        //Triem el color del "track".
        g.setColor(trackColor);
        ((Graphics2D)g).fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
    }

    @Override
    //Creamos el boton "invisible" de decremento.
    protected JButton createDecreaseButton(int orientation) {
        return b;
    }

    @Override
    //Creamos el boton "invisible" de incremento.
    protected JButton createIncreaseButton(int orientation) {
        return b;
    }
}