package view.dialogs;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Classe para crear un JDialog de alerta generico a partir de un mensaje
 *
 * @author : David
 */
public class AlertDialog extends JDialog {

    /**
     * constructor de la clase
     * @param error error a mostrar en el dialog
     */
    public AlertDialog(String error){
        final JPanel jpError = new JPanel(new GridBagLayout());

        setLocationRelativeTo(null);
        setAlwaysOnTop(true);

        /* Afegim Icono de finestra */
        try {
            Image image = ImageIO.read(new File("resources/logo.png"));
            this.setIconImage(image);
        } catch (IOException e) {
            System.out.println("Could not load the icon of the window.");
        }
        /* Afegim Imatge a Dialog */
        try {
            jpError.add(new JLabel(new ImageIcon(ImageIO.read(new File("resources/warning.png")))));
        } catch (Exception e) {
            System.out.println("No s'ha pogut carregar la imatge.");
        }

        this.setSize(400, 150);
        jpError.add(new JLabel(error));
        this.getContentPane().add(jpError);
    }
}
