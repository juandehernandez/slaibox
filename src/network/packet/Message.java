package network.packet;

import util.CurrentDate;

/**
 * Clase que representa un mensaje ha un usuario.
 *
 * @author Daniel Ortiz
 * @version 1.1.0
 */
public class Message extends Packet {

    //constantes publicas
    public static final String EMPTY_MESSAGE = "";
    public static final String EMPTY_PATH = "";

    private int idChannel;
    private int messageId;

    /**
     * Contructor de la clase
     * @param userEmail email del usuario que manda el mensaje
     * @param idChannel id del canal al que pertenece el mensaje
     */
    public Message(String userEmail, int idChannel) {
        super(userEmail);
        this.idChannel = idChannel;
    }

    /**
     * Contructor de la clase
     * @param userEmail email del usuario que manda el mensaje
     * @param idChannel id del canal al que pertenece el mensaje
     * @param messageId id del mensaje enviado
     */
    public Message(String userEmail, int idChannel, int messageId) {
        super(userEmail);
        this.idChannel = idChannel;
        this.messageId = messageId;
    }

    /**
     * Contructor de la clase
     * @param messageId id del mensaje enviado
     */
    public Message(int messageId) {
        this.messageId = messageId;
    }

    /**
     *Devuelve el id del canal del mensaje
     * @return id del canal
     */
    public int getIdChannel() {
        return idChannel;
    }

    /**
     * Indica si el mensaje recibido es un nuevo mensaje
     * @param lastMessage ultimo mensaje recibido
     * @param newMessage nuevo mensaje
     * @return true si el mensaje es nuevo o false si no es nuevo
     */
    public static boolean isNewMessage(Message lastMessage, Message newMessage){
        if (lastMessage == null) return true;
        else if (!lastMessage.getEmailUser().equals(newMessage.getEmailUser())) return true;
        else if (CurrentDate.nextDay(lastMessage.getDate(),newMessage.getDate())) return true;
        else return false;
    }

    /**
     * Devuelve el id del mensaje
     * @return id del mensaje
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * Set del id del mensaje
     * @param messageId
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }
}
