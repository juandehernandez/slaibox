package network.packet.request;

import network.packet.Packet;

/**
 * Clase para pedir un archivo al servidor
 *
 * @author Matias
 */
public class FileRequest extends Packet {

    private int fileId;
    private int channelId;
    private String channelName;

    /**
     * Constructor del FileRequest
     * @param fileId id del archivo que se pide
     * @param channelId id del canal al que pertenece el archvio que se pide
     * @param channelName nombre del canal al que pertenece el archivo que se pide
     */
    public FileRequest(int fileId, int channelId, String channelName){
        super();
        this.fileId = fileId;
        this.channelId = channelId;
        this.channelName = channelName;
    }

    /**
     * Devuelve el id del canal
     * @return id del canal
     */
    public int getChannelId() {
        return channelId;
    }

    /**
     * Devuelve el id del archivo
     * @return id del archivo
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * Devuelve el nombre del canal
     * @return nombre del canal
     */
    public String getChannelName() {
        return channelName;
    }
}
