package network.packet.request;


import model.AESCrypt;
import network.packet.Packet;

/**
 * Clase que representa la peticion de login de un usuario
 *
 * @author Daniel
 * @version 1.1.0
 */
public class Login extends Packet {
    private String password;

    /**
     * Constructor
     * @param user usuario que hace la peticion
     * @param password contrasena
     */
    public Login(String user, char[] password) {
        super(user);
        this.password = AESCrypt.encrypt(password);
    }

    /**
     * Devuelve la contraseña
     * @return contraseña
     */
    public String getPassword() {
        return password;
    }
}
