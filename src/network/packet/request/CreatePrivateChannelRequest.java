package network.packet.request;

import network.packet.Packet;

/**
 * Paquete para peticion de crear un canal privado con otro usuario
 *
 * @author Sergi
 */
public class CreatePrivateChannelRequest extends Packet {
    private String otherEmail;

    /**
     * Constructor
     * @param emailUser email del que hace la peticion
     * @param otherEmail email del otro usuario
     */
    public CreatePrivateChannelRequest(String emailUser, String otherEmail) {
        super(emailUser);
        this.otherEmail = otherEmail;
    }

    /**
     * Getter del email del otro
     * @return email del otro
     */
    public String getOtherEmail() {
        return otherEmail;
    }
}
