package network.packet.response;

import network.packet.File;
import network.packet.Packet;

/**
 * Clase para enviar un mensaje al cliente
 *
 * @author Matias
 */
public class FileRequestResponse extends Packet {

    private File file;

    /**
     * Constructor del FileRequestResponse
     * @param file archivo que devuelve la peticion
     */
    public FileRequestResponse(File file){
        super();
        this.file = file;
    }

    /**
     * Deuelve un archivo
     * @return archivo
     */
    public File getFile() {
        return file;
    }
}
