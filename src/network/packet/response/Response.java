package network.packet.response;

import network.packet.Packet;

/**
 * Clase para contestar si ha ido bien o mal
 *
 * @author Miguel
 */
public class Response extends Packet {

    private String response;
    private Packet packet;
    private boolean isError;

    /**
     * Constructor
     * @param response tipo de respuesta
     */
    public Response(String response) {
        this(response, null);
    }

    /**
     * Constructor
     * @param response tipo de respuesta
     * @param packet packete al que se responde
     */
    public Response(String response, Packet packet) {
        this.response = response;
        this.packet = packet;
        isError = response.contains("!");
    }

    /**
     * Getter de la respuesta
     * @return respuesta
     */
    public String getResponse() {
        return response;
    }

    /**
     * Setter de la respuesta
     * @param response repuesta
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * Getter del packet
     * @return packet
     */
    public Packet getPacket() {
        return packet;
    }

    /**
     * Setter del packet
     * @param packet packet
     */
    public void setPacket(Packet packet) {
        this.packet = packet;
    }

    /**
     * Devuelve si es una respuesta de error
     * @return si es una respuesta de error o no
     */
    public boolean isError() {
        return isError;
    }
}