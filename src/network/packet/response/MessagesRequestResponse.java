package network.packet.response;

import network.packet.Message;
import network.packet.Packet;

import java.util.List;

/**
 * Envio de mensajes de un canal a un cliente
 *
 * @author Sergi
 */
public class MessagesRequestResponse extends Packet {
    private List<Message> messages;
    private int channelId;
    private int offset;
    private String channelName;

    /**
     * Constructor
     * @param messages lista de mensajes
     * @param channelName nombre del canal
     * @param channelId id del canal
     * @param offset offset
     */
    public MessagesRequestResponse(List<Message> messages, String channelName, int channelId, int offset) {
        this.messages = messages;
        this.channelName = channelName;
        this.channelId = channelId;
        this.offset = offset;
    }

    /**
     * Getter de la lista de mensajes de la respuesta
     * @return lista de mensajes de la respuesta
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Getter de las ids del canal de donde son los mensajes
     * @return lista de id de los canales
     */
    public int getChannelId() {
        return channelId;
    }

    /**
     * Getter del offset de los mensajes
     * @return offset de los mensajes
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Getter del nombre del canal
     * @return nombre del canal
     */
    public String getChannelName() {
        return channelName;
    }
}
