package network.packet.response;

import network.packet.Channel;
import network.packet.Packet;
import network.packet.Profile;

import java.util.List;

/**
 * Avisa de que se ha añadido un usuario a un canal
 *
 * @author Sergi
 */
public class UserAddedToChannel extends Packet {
    private Profile profile;
    private Channel channel;
    private List<Profile> profiles;

    /**
     * Constructor
     * @param profile profile del usuario añadido
     * @param channel canal al que se ha añadido el usuario
     */
    public UserAddedToChannel(Profile profile, Channel channel) {
        this.profile = profile;
        this.channel = channel;
    }

    /**
     *
     * @param profile profile del usuario añadido
     * @param channel canal al que se ha añadido el usuario
     * @param profiles otros perfiles que hay en el canal
     */
    public UserAddedToChannel(Profile profile, Channel channel, List<Profile> profiles) {
        this.profile = profile;
        this.channel = channel;
        this.profiles = profiles;
    }

    /**
     * Getter del perfil que se ha añadido
     * @return perfil añadido
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * Getter del canal al que se añade el usuario
     * @return canal al que se ha añadido
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Getter de los perfiles que hay en el canal
     * @return lista de perfiles del canal
     */
    public List<Profile> getProfiles() {
        return profiles;
    }

    /**
     * Setter de la lista de perfiles del canal
     * @param profiles lista de perfiles del canal
     */
    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }
}