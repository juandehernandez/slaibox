package network.packet;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Clase que se encarga de la información de perfil de un usuario.
 *
 * @autor Miguel Abellán
 * @version 1.1
 */
public class Profile implements Serializable{

    public static final String CONNECTED = "Connected";
    public static final String OCCUPIED = "Occupied";
    public static final String DELETED = "Deleted";
    public static final String DISCONNECTED = "Disconnected";

    private String username;
    private String email;
    private String status;
    private ImageIcon profileImage;
    private List<Integer> idChannels;
    private Date lastConnectionDate;
    private Date registrationDate;

    /**
     * Constructor
     * @param username username
     * @param email email
     * @param profileImage imagen de perfil
     * @param status status
     * @param idChannels id canal
     * @param lastConnectionDate ultima conexion
     * @param registrationDate fecha de registro
     */
    public Profile(String username, String email, BufferedImage profileImage, String status, List<Integer> idChannels, Date lastConnectionDate, Date registrationDate){
        this.username = username;
        this.email = email;
        this.status = status;
        this.profileImage = new ImageIcon(profileImage);
        this.idChannels = new ArrayList<>(idChannels);
        this.lastConnectionDate = lastConnectionDate;
        this.registrationDate = registrationDate;
    }

    /**
     * Getter del username
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter del username
     * @param username username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter del email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter del email
     * @param email email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter de la imagen de perfil
     * @return imagen del perfil
     */
    public ImageIcon getProfileImage() {
        return profileImage;
    }

    /**
     * Setter del profile image
     * @param profileImage profile image
     */
    public void setProfileImage(BufferedImage profileImage) {
        this.profileImage = new ImageIcon(profileImage);
    }

    /**
     * Getter de la ultima fecha de connexion
     * @return ultima fecha de conexion
     */
    public Date getLastConnectionDate() {
        return lastConnectionDate;
    }

    /**
     * Getter de la fecha de registro
     * @return
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Getter de los id de los canales
     * @return lista de ids de los canales
     */
    public List<Integer> getIdChannels() {
        return idChannels;
    }

    /**
     * Setter de los id de los canales
     * @param idChannels id de los canales
     */
    public void setIdChannels(List<Integer> idChannels) {
        this.idChannels = idChannels;
    }

    /**
     * Getter del status
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter del status
     * @param status status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}

