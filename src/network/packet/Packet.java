package network.packet;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que empaqueta un dato a enviar, clase padre de cualquier trama.
 *
 * @author Daniel Ortiz
 * @version 1.2.0
 */
public abstract class Packet implements Serializable {

    protected Date date;
    protected String emailUser;

    public Packet(String emailUser) {
        date = new Date();
        this.emailUser = emailUser;
    }

    /**
     * Constructor de la clase
     */
    public Packet() {
        this(null);
    }

    /**
     * Devuelve el dia
     * @return el dia
     */
    public Date getDate() {
        return date;
    }

    /**
     * Devuelve el email del usuario
     * @return el email del usuario
     */
    public String getEmailUser() {
        return emailUser;
    }

    /**
     * Set del dia
     * @param date dia
     */
    public void setDate(Date date){
        this.date = date;
    }

    /**
     * Set del email del usaurio
     * @param emailUser
     */
    public void setEmailUser(String emailUser){
        this.emailUser = emailUser;
    }
}
