package network.packet;

import java.util.ArrayList;

/**
 * Clase que se encarga de la información de un canal.
 *
 * @author Miguel Abellán
 * @version 1.7
 */
public class Channel extends Packet {

    private String name;
    private int id;
    private boolean isPrivate;
    private String description;
    private ArrayList<String> users;
    private ArrayList<Message> messages;
    private int lastViewedMessageId;
    private ArrayList<File> filesInfo;
    private int totalMessagesNumber;
    private int messageOffset;
    private int unreadMessagesNumber;

    /**
     * Contructor de la clase
     * @param name nombre del canal
     * @param description descripcion del canal
     * @param users usuarios del canal
     * @param isPrivate indica si el canal es privado o no (True privado, False publico)
     * @param id id del canal
     * @param messages mensaje que contiene el canal
     */
    public Channel(String name, String description, ArrayList<String> users, boolean isPrivate, int id,
                   ArrayList<Message> messages) {
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
        this.id = id;
        this.messages = messages;
        this.users = users;
        filesInfo = null;
        totalMessagesNumber = messageOffset = unreadMessagesNumber = 0;

    }


    /**
     * * Contructor de la clase
     * @param channelName nombre del canal
     * @param channelId id del canal
     * @param lastViewedMessageId id del ultimo mensaje leido del canal
     */
    public Channel(String channelName, int channelId, int lastViewedMessageId)  {
        this.name = channelName;
        this.id = channelId;
        this.lastViewedMessageId = lastViewedMessageId;
    }

    /**
     * Contructor de la clase
     * @param name nombre del canal
     * @param description descripcion del canal
     * @param users usuarios del canal
     * @param isPrivate indica si el canal es privado o no (True privado, False publico)
     * @param id id del canal
     * @param messages mensaje que contiene el canal
     * @param lastViewedMessageId id del ultimo mensaje leido del canal
     * @param filesInfo informacion de los archivos (sin los datos)
     * @param totalMessagesNumber numero total de mensajes que tiene el canal
     */
    public Channel(String name, String description, ArrayList<String> users, boolean isPrivate, int id,
                   ArrayList<Message> messages, int lastViewedMessageId, ArrayList<File> filesInfo, int totalMessagesNumber) {
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
        this.id = id;
        this.messages = messages;
        this.users = users;
        this.lastViewedMessageId = lastViewedMessageId;
        this.filesInfo = filesInfo;
        this.totalMessagesNumber = totalMessagesNumber;
    }

    /**
     * Devuelve la informacion de los archivos (sin los datos)
     * @return informacion del archivo
     */
    public ArrayList<File> getFilesInfo() {
        return filesInfo;
    }

    /**
     * Set de la informacion de los archivos
     * @param filesInfo informacion de los archivos
     */
    public void setFilesInfo(ArrayList<File> filesInfo) {
        this.filesInfo = filesInfo;
    }

    /**
     * Devuelve el numero total de mensajes que hay en el canal
     * @return numero total de mensajes que hay en el canal
     */
    public int getTotalMessagesNumber() {
        return totalMessagesNumber;
    }

    /**
     * Set del numero total de mensajes que hay en el canal
     * @param totalMessagesNumber numero total de mensajes que hay en el canal
     */
    public void setTotalMessagesNumber(int totalMessagesNumber) {
        this.totalMessagesNumber = totalMessagesNumber;
    }

    /**
     * Incrementa el offset de los mensajes
     */
    public void incrementOffset(){
        messageOffset++;
    }

    /**
     * Incrementa el numero total de los mensajes
     */
    public void incrementTotalMessages() {
        totalMessagesNumber++;
    }

    /**
     * Incrementa el numero de mensajes no leidos
     */
    public void incrementUnreadMessages() {
        unreadMessagesNumber++;
    }

    /**
     * Devuelve el offset de los mensajes
     * @return
     */
    public int getMessageOffset() {
        return messageOffset;
    }

    /**
     * Set del offset de los mensajes
     * @param messageOffset offset de los mensajes
     */
    public void setMessageOffset(int messageOffset) {
        this.messageOffset = messageOffset;
    }

    /**
     * Devuelve el numero de mensajes sin leer
     * @return el numero de mensajes sin leer
     */
    public int getUnreadMessagesNumber() {
        return unreadMessagesNumber;
    }

    /**
     * Set del numero de mensajes sin leer
     * @param unreadMessagesNumber  numero de mensajes sin leer
     */
    public void setUnreadMessagesNumber(int unreadMessagesNumber) {
        this.unreadMessagesNumber = unreadMessagesNumber;
    }

    /**
     * Set de los mensajes del canal
     * @param messages mensajes del canal
     */
    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    /**
     * Set del id del ultimo mensaje visto del canal
     * @param lastViewedMessageId id del ultimo mensaje visto del canal
     */
    public void setLastViewedMessageId(int lastViewedMessageId) {
        this.lastViewedMessageId = lastViewedMessageId;
    }

    /**
     * Devuelve id del ultimo mensaje visto del canal
     * @return id del ultimo mensaje visto del canal
     */
    public int getLastViewedMessageId() {
        return lastViewedMessageId;
    }

    /**
     * Devuelve el nombre del canal
     * @return nombre del canal
     */
    public String getName() {
        return name;
    }

    /**
     * Set del nombre del canal
     * @param name el nombre del canal
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Devuelve la descripcion del canal
     * @return descripcion del canal
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set de la descripcion del canal
     * @param description descripcion del canal
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Devuelve los usuarios que tiene el canal
     * @return usuarios que tiene el canal
     */
    public ArrayList<String> getUsers() {
        return users;
    }

    /**
     * Set de los usuarios que tiene el canal
     * @param users usuarios que tiene el canal
     */
    public void setUsers(ArrayList<String> users) {
        this.users = users;
    }

    /**
     * Devuelve si el canal es privado
     * @return true si el canal es privado o false si el canal es publico
     */
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * Devuelve el id del canal
     * @return
     */
    public int getId(){
        return id;
    }

    /**
     * Devuelve los mensajes del canal
     * @return mensajes del canal
     */
    public ArrayList<Message> getMessages() {
        return messages;
    }
}