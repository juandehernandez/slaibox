package network.packet;

/**
 * Clase que se encarga de la información referente a un mensaje de texto. La clase hereda de Message.
 *
 * @author Sergi Valbuena
 * @version 1.1
 */
public class Text extends Message {

    private String msg;

    /**
     * Constructor de la clase
     * @param emailUser correo del usuario
     * @param idChannel id de canal del mensaje
     * @param msg mensaje
     * @param messageId id del mensaje
     */
    public Text(String emailUser, int idChannel, String msg, int messageId) {
        super(emailUser, idChannel, messageId);
        this.msg = msg;
    }

    /**
     * Constructor de la clase
     * @param emailUser correo del usuario
     * @param idChannel id de canal del mensaje
     * @param msg mensaje
     */
    public Text(String emailUser, int idChannel, String msg) {
        super(emailUser, idChannel);
        this.msg = msg;
    }

    /**
     * Devuelve el mensaje
     * @return
     */
    public String getMessage() {
        return msg;
    }
}
