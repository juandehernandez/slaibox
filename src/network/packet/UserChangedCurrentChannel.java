package network.packet;

/**
 * Clase que informa de cambio de canal de un usario
 *
 * @author Sergi
 */
public class UserChangedCurrentChannel extends Packet {
    private int newCurrentChannel;

    /**
     * Constructor de la clase
     * @param userEmail mail del usuario nuevo
     * @param newCurrentChannel id del canal nuevo
     */
    public UserChangedCurrentChannel(String userEmail, int newCurrentChannel) {
        super(userEmail);
        this.newCurrentChannel = newCurrentChannel;
    }

    /**
     * Devuelve el id del canal nuevo
     * @return id del canal nuevo
     */
    public int getNewCurrentChannel() {
        return newCurrentChannel;
    }
}
