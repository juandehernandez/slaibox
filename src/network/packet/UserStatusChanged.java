package network.packet;

/**
 * Informa del cambio de estado de un usario
 *
 * @author Sergi
 */
public class UserStatusChanged extends Packet {
    private String status;

    /**
     * Constructor de la clase
     * @param emailUser mail del usuario
     * @param status estado al que cambia el usuario
     */
    public UserStatusChanged(String emailUser, String status) {
        super(emailUser);
        this.status = status;
    }

    /**
     * Devuelve el estado del usuario
     * @return estado del usuario
     */
    public String getStatus() {
        return status;
    }
}
