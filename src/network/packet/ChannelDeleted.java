package network.packet;

/**
 * Dice que un canal ha sido eliminado
 *
 * @author Sergi
 */
public class ChannelDeleted extends Packet {
    private int channelId;
    private String channelName;

    /**
     * constructor de la clase
     * @param channelId id del canal a eliminar
     * @param channelName nombre del canal a eliminar
     */
    public ChannelDeleted(int channelId, String channelName) {
        this.channelId = channelId;
        this.channelName = channelName;
    }

    /**
     * Devuelve el id del canal eliminado
     * @return id del canal eliminado
     */
    public int getChannelId() {
        return channelId;
    }

    /**
     * Devuelve el nombre del canal eliminado
     * @return nombre del canal eliminado
     */
    public String getChannelName() {
        return channelName;
    }
}
