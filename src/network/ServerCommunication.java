package network;


import controller.MainController;
import model.MainModel;
import network.packet.*;
import network.packet.request.*;
import network.packet.response.FileRequestResponse;
import network.packet.response.MessagesRequestResponse;
import network.packet.response.Response;
import network.packet.response.UserAddedToChannel;
import util.FileUtils;
import view.dialogs.AlertDialog;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Clase encargada de hablar con el servidor y gestionarlo
 *
 * @author Slaibox
 * @version 7.4.1
 */
public class ServerCommunication extends Thread {

    private static final String HOST = "localhost";
    private static final int PORT = 3344;
    private static final int HEARTBEAT_TIME = 1;

    private String host;
    private int port;
    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private MainController controller;
    private MainModel mainModel;
    private boolean running;
    private boolean connectionError;

    /**
     * Establece la coneccion con el servidor
     */
    public ServerCommunication(){
        connectionError = false;
        running = true;
        startConnection();
    }

    /**
     * Constructor que recibe un host y un puerto al cual conectarse
     * @param host donde conectarse
     * @param port al cual conectarse
     */
    public ServerCommunication(String host, int port){
        connectionError = false;
        running = true;
        this.host = host;
        this.port = port;
        startConnection();
    }

    /**
     * Empieza la comunicacion con el servidor
     */
    private void startConnection() {
        try {
            //Establecer comunicacion con el servidor y empezar el Thread para escuchar.
            socket = new Socket(host, port);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            this.start();


        } catch (ConnectException e1) {
            connectionError = true; //boolean que permite posteriormente cuando el login este creado mostrar
                                    //un JDialog de error
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Cierra la comunicacion con el servidor cerrando nuestro socket
     */
    public void stopConnection(){
        //heartbeat.stopHeartBeat();
        try {
            ArrayList<Channel> channels = new ArrayList<>();
            mainModel.getPrivateChannels().forEach((k,v) -> channels.add(new Channel(v.getName(), v.getId(), v.getLastViewedMessageId())));
            mainModel.getPublicChannels().forEach((k,v) -> channels.add(new Channel(v.getName(), v.getId(), v.getLastViewedMessageId())));
            out.writeObject(new Logout(mainModel.getProfile().getEmail(), channels));
            running = false;
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de escuchar al servidor
     */
    @Override
    public void run() {
        while (running) {
            try {
                Packet message = (Packet) in.readObject();
                if(message instanceof Response) {
                    responseRecieved((Response) message);
                } else if (message instanceof InitData){
                   initDataRecieved((InitData) message);
                } else if (message instanceof Text) {
                    textRecieved((Text) message);
                } else if (message instanceof File) {
                    fileRecieved((File) message);
                } else if (message instanceof  Logout) {
                    logoutRecieved((Logout) message);
                } else if (message instanceof MessagesRequestResponse) {
                    messagesRequestResponseRecieved((MessagesRequestResponse) message);
                } else if (message instanceof UserAddedToChannel) {
                    userAddedToChannelRecieved((UserAddedToChannel) message);
                } else if (message instanceof UserStatusChanged) {
                    userStatusChangedReceived((UserStatusChanged) message);
                } else if (message instanceof MessageUnread) {
                    messageUnreadReceived((MessageUnread) message);
                } else if (message instanceof ChannelDeleted) {
                    channelDeletedReceived((ChannelDeleted) message);
                } else if (message instanceof FileRequestResponse){
                    fileResponseReceived((FileRequestResponse) message);
                }
            } catch (ClassNotFoundException | IOException e) {
                System.out.println("Socket cerrado...");
                //El socket se ha cerrado, hay que hacer algo?
                if(!socket.isClosed()) {
                    System.out.println("El socket se ha cerrado!!!");
                    running = false;
                    try {
                        socket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
    }


    /**
     * Recibe un mensaje con el archivo solicitado al servidor
     * @param message archivo solicitado al servidor
     */
    private void fileResponseReceived(FileRequestResponse message) {
        FileUtils.donwloadFile(message.getFile(), controller.getView());
    }

    /**
     * Gestiona que un channel se haya eliminado desde el servidor
     *
     * @param p paquete con la informacion
     */
    private void channelDeletedReceived(ChannelDeleted p) {
        boolean currentChannel = mainModel.deleteChannel(p.getChannelId());
        controller.deleteChannelFromView(p.getChannelId(),currentChannel);
    }

    /**
     * Gestiona que un usario se ha agregado a un canal
     *
     * @param response el paquete con la informacion pertinente
     */
    private void userAddedToChannelRecieved(UserAddedToChannel response) {
        mainModel.insertProfiles(response.getProfiles());

        if(response.getProfile().getEmail().equals(mainModel.getProfile().getEmail())){
            /* Han afegit al nostre usuari a un canal nou */
            String channel = mainModel.addChannel(response.getChannel());
            boolean isPrivate = response.getChannel().isPrivate();
            int idChannel = response.getChannel().getId();
            controller.addChannelToList(channel,isPrivate,idChannel);
            if(isPrivate){
                String status = mainModel.getStatus(channel);
                controller.changeConnection(idChannel,status);
                if(mainModel.loadChannel(idChannel)){
                    controller.loadPrivate(channel,idChannel);
                }
            }
        }else{
            /* Usuari desconegut afegir a un nou canal */
            boolean isCurrent = mainModel.addUserToChannel(response.getProfile(),response.getChannel().getId());
            if(isCurrent){
                controller.incrementChannelUsers();
            }
        }
    }

    /**
     * Gestiona el paquete de cuando se recibe un mensaje no leido
     *
     * @param p paquete con la informacion
     */
    private void messageUnreadReceived(MessageUnread p) {
        controller.incrementChannel(p.getChannelId());
    }


    /**
     * Se encarga de cambiar el status de un usario cualquiera
     *
     * @param p paquete con la informacion
     */
    private void userStatusChangedReceived(UserStatusChanged p) {
        if (mainModel != null) {
            int idChannel = mainModel.updateStatus(p.getEmailUser(), p.getStatus());
            if (idChannel != -1) {
                controller.changeConnection(idChannel, p.getStatus());
            }
            controller.infoChannelChanged();
        }
    }

    /**
     * Cuando un Response se recibe, actua en funcion de ello
     *
     * @param r la Response
     */
    private void responseRecieved(Response r) {
        Packet p = r.getPacket();
        if (p != null) {
            if (p instanceof Register) {
                controller.showMessage(r.getResponse(),
                        r.isError() ? JOptionPane.WARNING_MESSAGE : JOptionPane.INFORMATION_MESSAGE);
                if (!r.isError()) {
                    controller.closeRegister();
                }
            } else if (p instanceof Login) {
                if (controller != null ){
                    if(r.isError()) {
                        controller.enableLogin();
                        controller.showMessage(r.getResponse(), JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        }
    }

    /**
     * Gestiona toda la informacion de inicializacion del programa
     *
     * @param initData el paquete con la informacion
     */
    private void initDataRecieved(InitData initData) {
        controller.showMainView();
        controller.closeLogin();
        mainModel.addAllInfo(initData);
        mainModel.searchConnexionUsers();
        controller.closeLogin();
        controller.showMainView();
        mainModel.getPrivateChannels()
                .forEach((k, v) -> {
                    if (v.getUnreadMessagesNumber() > 0) {
                        controller.setUnseenMessage(v.getId(), v.getUnreadMessagesNumber());
                    }
                });
        mainModel.getPublicChannels()
                .forEach((k, v) -> {
                    if (v.getUnreadMessagesNumber() > 0) {
                        controller.setUnseenMessage(v.getId(), v.getUnreadMessagesNumber());
                    }
                });
    }

    /**
     * Recibe un mensaje de texto
     * guarda el mensaje
     *
     * muestra el mensaje en el chat
     * @param text mensaje de texto recibido
     */
    private void textRecieved(Text text) {
        Channel actualChannel = mainModel.getCurrentChannel();
        Message lastMessage = null;

        //verificar si el mensaje le llega al canal actual

        if (actualChannel != null && text.getIdChannel() == actualChannel.getId()){
            if (actualChannel.getMessages().size() > 0) {
                lastMessage = actualChannel.getMessages().get(actualChannel.getMessages().size() - 1);
            }
            //buscar perfil actual
            controller.loadChannelText(lastMessage, text, mainModel.getProfileFromEmail(text.getEmailUser()).getProfileImage(), actualChannel.getLastViewedMessageId());
        }
        //mandar el mensaje nuevo al modelo
        mainModel.addNewMessage(text);
        if(actualChannel != null) {
            if (actualChannel.getId() == text.getIdChannel()) {
                controller.infoChannelChanged();
            }
        }
    }

    /**
     * Recibe un archivo del servidor
     * guarda el archivo
     * muestra el archivo en el chat
     *
     * @param file archivo recibido
     */
    private void fileRecieved(File file) {
        Channel actualChannel = mainModel.getCurrentChannel();
        Message lastMessage = null;

        //verificar si el mensaje le llega al canal actual
        if (actualChannel != null && file.getIdChannel() == actualChannel.getId()) {
            if (actualChannel.getMessages().size() > 0) {
                lastMessage = actualChannel.getMessages().get(actualChannel.getMessages().size() - 1);
            }

            controller.loadChannelFile(lastMessage, file, mainModel.getProfileFromEmail(file.getEmailUser()).getProfileImage(), actualChannel.getLastViewedMessageId());
                        /*Mensaje nuevo*/

            //mandar el mensaje nuevo al modelo
        }
        mainModel.addFile(file);
        if(actualChannel != null) {
            if (actualChannel.getId() == file.getIdChannel()) {
                controller.infoChannelChanged();
            }
        }
    }


    /**
     * Gestiona la respuesta del servidor cuando este dice que hay que desconectarse
     *
     * @param logout paquete con la informacion
     * @throws IOException en caso de que no se pueda cerrar el socket
     */
    private void logoutRecieved(Logout logout) throws  IOException {
        running = false;
        socket.close();
        new AlertDialog("Connection Error: Server Closed.").setVisible(true);
        controller.unableLogin();
    }

    /**
     * Recibe la respuesta de la solicitud de mas mensajes
     * Muestra los mensajes obtenidos de la solicitud en el canal
     * @param response respuesta del servidor que contiene los mensajes
     */
    private void messagesRequestResponseRecieved(MessagesRequestResponse response) {
        mainModel.addNewMessages(response);
        Channel currentChannel = mainModel.getCurrentChannel();

        if (currentChannel.getId() == response.getChannelId()) {
            controller.loadChannelMessages(currentChannel.getMessages(), currentChannel.getLastViewedMessageId());
        }

        //blouear boton de enviar mas mensajes
        if(currentChannel.getMessages() != null) {
            if (currentChannel.getMessages().size() >= currentChannel.getTotalMessagesNumber() - 1) {
                controller.setEnableRequestMoreMessages(false);
            }
        }else{
            /* Canal no creado */
            controller.setEnableRequestMoreMessages(false);
        }
    }


    /**
     * Envia informacion al servidor
     * @param packet que se envia al servidor
     */
    public void sendPacket(Packet packet){

        try {
            out.writeObject(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia los datos de login al servidor
     * @param username usuario o correo electronico
     * @param password contraseña del usuario
     */
    public void sendLogin(String username, char[] password) {
        Login loginData = new Login(username, password);
        try {
            out.writeObject(loginData);
            controller.unableLogin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia los datos de registro al servidor
     *
     * @param email del usuario que se registra
     * @param username nombre del usuario que se registra
     * @param password contraseña del usuario que se registra
     * @param confirmPassword comprobacion de la conraseña del usuario que se registra
     */
    public void sendRegister(String email, String username, char[] password, char[] confirmPassword){

        Register registerData = new Register(username, email,password,confirmPassword);
        try {
            out.writeObject(registerData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Notifica al servidor que el usuario se cambio de canal
     * @param channel id del nuevo canal
     */
    public void userChangedChannel(Channel channel) {
        UserChangedCurrentChannel packet =
                new UserChangedCurrentChannel(mainModel.getProfile().getEmail(),
                                           channel.getId());
        sendPacket(packet);
    }

    /**
     * Envia un mensaje al servidor
     * @param message mensaje a enviar
     * @param user_email usuario que manda el archivo
     * @param id_canal id del canal desde el cual se envia el mensaje
     */
    public void sendText(String message, String user_email, int id_canal){
        boolean end = false;
        int i = 0;
        int primera = 0;
        while (!end){
            if(message.length() > 100 + (100*i) + 2 * primera){
                message = message.substring(0,100 + 100*i + 2 * primera) + System.getProperty("line.separator") + message.substring(100 + 100*i + 2 * primera, message.length());
                i++;
                primera = 1;
            }else{
                end = true;
            }
        }
        if(message.length() > 1000){
            message = message.substring(0,1000);
            new AlertDialog("Message was too long (truncating)").setVisible(true);
        }
        Text messagePacket = new Text(user_email,id_canal, message);
        try {
            out.writeObject(messagePacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Envia un archivo al servidor
     * @param user usuario que manda el archivo
     * @param data file a mandar, puede ser imagen o cualquier archivo
     * @param name nombre del archivo
     * @param idChannel identificador del canal actual
     */
    public void sendFile(String user, byte[] data, String name, int idChannel){

        network.packet.File f = new network.packet.File(user,data,name, idChannel);
        //Verificamos si el un archivo o una imagen
        String ext = f.getExt();
        switch (ext){
            case "png":
            case "jpg":
            case "jpeg":
            case "bmp":
                network.packet.File img = new network.packet.File(user,data,name,idChannel);
                try {
                    out.writeObject(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            //Si no es ninguna de las extensiones, por defecto es un archivo
            default:
                try {
                    out.writeObject(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Envia Bytes al servidor
     */
    protected void sendByte(){

        try {
            out.writeByte(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set el main controller
     *
     * @param controller
     */
    public void setMainController(MainController controller) {
        this.controller = controller;
        if(connectionError) {
            this.controller.unableLogin();
            new AlertDialog("Connection Error: Verify that the server is running.").setVisible(true);
        }
    }

    /**
     * Set el main model
     *
     * @param mainModel
     */
    public void setMainModel(MainModel mainModel){
        this.mainModel = mainModel;
    }

    /**
     * Pide los mensahes de un canal deseado, a partir de un offset
     *
     * @param channelName el nombre del canal
     * @param channelId   id del canal
     * @param offset      offset deseado para obtener los mensajes
     */
    public void askForMessages(String channelName, int channelId, int offset) {
        try {
            out.writeObject(new MessagesRequest(channelName, channelId, offset));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Pide un archivo al servidor
     * @param fileId id del archivo a solicitar
     * @param channelId id del canal al cual pertenece el archivo
     * @param channelName nombre del canal al cual pertenece el archivo
     */
    public void requestFile(int fileId, int channelId, String channelName){
        try {
            out.writeObject(new FileRequest(fileId, channelId, channelName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
