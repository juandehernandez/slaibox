package main;

import controller.MainController;
import controller.credentials.LoginController;
import controller.credentials.RegisterController;
import controller.cvcontroller.CVButtonController;
import controller.cvcontroller.CVKeyListener;
import controller.cvcontroller.CVMouseController;
import controller.lateralcontroller.LateralDocumentController;
import controller.lateralcontroller.LateralMouseController;
import controller.lateralcontroller.UserViewController;
import model.MainModel;
import model.config.NetworkConfig;
import network.ServerCommunication;
import view.MainView;
import view.credentials.LoginView;
import view.credentials.RegisterView;
import view.dialogs.AlertDialog;

import javax.swing.*;
import java.io.FileNotFoundException;

/**
 * Punto principal de entrada del programa
 *
 * @version 2.0.0
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {

            //Buscar los datos del archivo.
            NetworkConfig nConfig;
            try {
                nConfig = NetworkConfig.initialConfig();

                ServerCommunication serverCom = new ServerCommunication(nConfig.getIp(),nConfig.getPort());

                RegisterView register = new RegisterView();
                RegisterController registerController = new RegisterController(register, serverCom);
                register.registerController(registerController);

                LoginView login = new LoginView();
                LoginController loginController = new LoginController(login, register, serverCom);
                login.registerController(loginController, loginController);
                login.setVisible(true);

                MainView view = new MainView();
                MainController controller = new MainController(view, loginController, registerController, serverCom);
                MainModel mainModel = new MainModel(controller, serverCom);
                serverCom.setMainController(controller);
                serverCom.setMainModel(mainModel);
                UserViewController userViewController = new UserViewController();
                userViewController.setViews(view.getUserView(), view.getMenuBarView());
                userViewController.setModel(mainModel);
                userViewController.setNetwork(serverCom);
                userViewController.setMainView(view);
				controller.setModel(mainModel);
                view.registerUserViewController(userViewController);

                serverCom.setMainController(controller);
                serverCom.setMainModel(mainModel);

                /* Middle controller*/
                CVButtonController cvButtonController = new CVButtonController(view, serverCom, mainModel);
                cvButtonController.setController(controller);
                CVMouseController cvMouseController = new CVMouseController(view, mainModel, serverCom);
                CVKeyListener cvKeyListener = new CVKeyListener(view,mainModel,serverCom);

                /* Lateral controller */
                LateralDocumentController ltrlDocController = new LateralDocumentController(
                        view.getJpBarraLateral(),mainModel);
                LateralMouseController ltrlController = new LateralMouseController(view,mainModel,
                        controller,ltrlDocController);

                controller.setCvButtonController(cvButtonController);
                controller.setLateralController(ltrlDocController,ltrlController);
                view.registerController(controller);
                view.registerContentViewControllers(cvButtonController, cvMouseController, cvKeyListener, cvMouseController, cvKeyListener);
                view.registerLateralControllers(ltrlController,ltrlDocController);
                view.registerInfoPanelController(cvButtonController,cvMouseController);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                AlertDialog alertDialog = new AlertDialog("¡Error al encontrar el archivo de configuración!");
                alertDialog.setVisible(true);
            }
        });
    }
}
