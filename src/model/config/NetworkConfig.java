package model.config;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Clase para la configuracion de comunicacion, a donde se conectara el cliente
 *
 * @author Matias
 */
public class NetworkConfig {

    private String ip;
    private int port;

    /**
     * Devuelve la ip a cual hay q conectarse
     * @return ip a cual conectarse
     */
    public String getIp() {
        return ip;
    }

    /**
     * Devuelve el puerto al cual hay que conectarse
     * @return el puerto al cual hay que conectarse
     */
    public int getPort() {
        return port;
    }


    /**
     * Parsea la informacion de archivo
     * @return NetworkConfig con la informacion del archivo de configuracion
     * @throws FileNotFoundException
     */
    public static NetworkConfig initialConfig() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader("config.json"));
        NetworkConfig networkConfig = gson.fromJson(jsonReader, NetworkConfig.class);
        return networkConfig;
    }
}
