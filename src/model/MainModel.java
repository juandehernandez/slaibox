package model;

import controller.MainController;
import network.ServerCommunication;
import network.packet.*;
import network.packet.response.MessagesRequestResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * General class that handles all the user information
 *
 * @author David
 */
public class MainModel extends Thread{

    private Profile profile;

    private HashMap<Integer, Channel> publicChannels;
    private HashMap<Integer, Channel> privateChannels;

    private HashMap<String, Profile> profiles;
    private MainController controller;
    private Channel currentChannel;
    private ServerCommunication serverCommunication;

    //variables para la busqueda de canales
    private String searchLabel;
    private boolean done;

    /**
     * Constructor
     * @param controller controlador principal
     * @param serverCommunication comuncacion con el servidor
     */
    public MainModel(MainController controller, ServerCommunication serverCommunication){
        publicChannels = new HashMap<>();
        privateChannels = new HashMap<>();

        this.controller = controller;
        currentChannel = null;
        this.serverCommunication = serverCommunication;

        searchLabel = null;
        done = true;
        start();
    }

    /**
     * Obtiene el perfil del cliente actual
     * @return perfil del cliente actual
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * Devuelve el canal abierto actualmente si es null no se ha abierto ningun canal
     * @return Devuelve toda la informacion del canal actual
     */
    public Channel getCurrentChannel(){
        return currentChannel;
    }

    /**
     * Añade al model toda la informacion al model
     * @param initData contiene toda la informacion del usuario logeado
     */
    public void addAllInfo(InitData initData){
        profile = initData.getProfile();
        profiles = initData.getUsers();
        if(profiles.size() == 0){
            profiles.put(profile.getEmail(),profile);
        }

        HashMap<Integer, Channel> allChannels = initData.getChannels();

        List<String> privateChannelsNames = new ArrayList<>();
        List<String> publicChannelsNames = new ArrayList<>();
        List<Integer> publicIds = new ArrayList<>();
        List<Integer> privateIds = new ArrayList<>();

        allChannels.forEach((k,v) -> {
            if(v.isPrivate()){
                ArrayList<String> users = v.getUsers();
                String otherUser = null;
                privateChannelsNames.add(getPrivateChannelName(v));
                privateIds.add(v.getId());
                privateChannels.put(v.getId(), v);
            }else{
                publicIds.add(v.getId());
                publicChannelsNames.add(v.getName());
                publicChannels.put(v.getId(), v);
            }
        });
        controller.showAllInfo(profile,publicChannelsNames,privateChannelsNames,publicIds,privateIds);
    }

    /**
     * Metodo para cambiar de canales
     * @param isPrivate Indica si es un chat 1 a 1
     * @param idChannel Indica el numero del canal
     * @return La informacion del canal correspondiente o null si el canal no existe
     */
    public Channel changeChannel(boolean isPrivate, int idChannel){
        if(isPrivate) {
            currentChannel = privateChannels.get(idChannel);
        }else {
            currentChannel = publicChannels.get(idChannel);
        }
        serverCommunication.userChangedChannel(currentChannel);
        return currentChannel;
    }

    /**
     * Retorna un file si existe en el modelo o null si no existe.
     * @param channelId
     * @param idFile
     * @return Retorna un file si existe en el modelo o null si no existe.
     */
    public File getFile(int channelId, int idFile, boolean isPrivate){
        File f = null;

        //buscar le channel en los canales privados o publicos
        Channel channel;
        if (isPrivate){
            channel = privateChannels.get(channelId);
        }else{
            channel = publicChannels.get(channelId);
        }
        //verificar que se encontro un channel
        if (channel != null){
            ArrayList<Message> channelMessages = channel.getMessages();
            //buscar el archivo en los mensajes
            for(Message m : channelMessages){
                if (m.getMessageId() == idFile){
                    //si se encuentra el archivo se devuelve
                    return (File) m;
                }
            }
        }
        //si no se encontro retorna false
        return f;
    }

    /**
     * Agrega un Mensaje a su respectivo canal
     * @param newMessage
     */
    public void addNewMessage(Message newMessage){
        addMessageToChannel(newMessage);
    }

    /**
     * Agrega un File a su respectivo canal
     * @param file
     */
    public void addFile(File file){
        //getCurrentChannel().getMessages().add(file);
        addMessageToChannel(file);
    }

    /**
     * Agrega un mensaje nuevo a su respectivo canal
     * @param newMessage
     */
    private void addMessageToChannel(Message newMessage){

        boolean found = false;
        int key = newMessage.getIdChannel();



        if (currentChannel != null && currentChannel.getId() == newMessage.getIdChannel()){
            currentChannel.getMessages().add(newMessage);
            if(newMessage instanceof File) {
                currentChannel.getFilesInfo().add((File) newMessage);
            }
            currentChannel.incrementTotalMessages();
        }else {
            //buscar el canal en los mensajes
            //Buscar si el canal es publico
            if (publicChannels.get(key) != null){
                if (publicChannels.get(key).getMessages() != null) {
                    publicChannels.get(key).getMessages().add(newMessage);
                }
                if(newMessage instanceof File) {
                    if (publicChannels.get(key).getFilesInfo() != null) {
                        publicChannels.get(key).getFilesInfo().add((File) newMessage);
                    }
                }
                publicChannels.get(key).incrementTotalMessages();
                publicChannels.get(key).incrementUnreadMessages();

            }else {
                //Buscar en canales privados
                if (privateChannels.get(key) != null){
                    if (privateChannels.get(key).getMessages() != null) {
                        privateChannels.get(key).getMessages().add(newMessage);
                    }
                    if(newMessage instanceof File) {
                        if (privateChannels.get(key).getFilesInfo() != null) {
                            privateChannels.get(key).getFilesInfo().add((File) newMessage);
                        }
                    }
                    privateChannels.get(key).incrementTotalMessages();
                    privateChannels.get(key).incrementUnreadMessages();
                }
            }
        }
    }

    /**
     * Devuelve un Profile a partir de un email
     * @param email
     * @return
     */
    public Profile getProfileFromEmail(String email) {
        return profiles.get(email);
    }

    /**
     * Pide al servidor mensajes antiguos de un canal
     * @param channelName nombre del canal
     * @param channelId id del canal
     * @param offset offset de los mensajes
     */
    public void askForMessages(String channelName, int channelId, int offset) {
        serverCommunication.askForMessages(channelName, channelId, offset);
    }

    /**
     * Agrega los 100 mensajes anteriores pedidos al servidor
     * @param messagesResponse
     */
    public void addNewMessages(MessagesRequestResponse messagesResponse) {
        //revertir el orden de la lista
        ArrayList<Message> messageAux = new ArrayList<>(messagesResponse.getMessages());
        Collections.reverse(messageAux);

        Channel targetChannel;
        targetChannel = publicChannels.get(messagesResponse.getChannelId());
        if (targetChannel == null) {
            targetChannel = privateChannels.get(messagesResponse.getChannelId());
        }
        if (targetChannel.getMessages() == null) {
            targetChannel.setMessages(messageAux);
        } else {
            targetChannel.getMessages().addAll(0, messageAux);
        }
    }

    /**
     * Cambia el estado del perfil
     *
     * @param status al cual cambiar
     */
    public void setStatus(String status){
        profile.setStatus(status);
        profiles.get(profile.getEmail()).setStatus(status);
        controller.channelChanged();
    }


    /**
     * Buscar que canales contienen una palabra clave
     *
     * @param contenido palabra clave a buscar
     */
    public void getCanalesNoSimilares(String contenido){
        searchLabel = contenido;
        done = false;
    }

    /**
     * Ejecuta el thread de filtrar los canales por texto
     */
    @Override
    public void run(){
        while (true) {
            if(!done) {
                ArrayList<Integer> list = new ArrayList<>();
                publicChannels.forEach((k, v) -> {
                    if (!v.getName().contains(searchLabel)) {
                        list.add(v.getId());
                    }
                });
                privateChannels.forEach((k, v) -> {
                    List<String> users = v.getUsers();
                    for(int i = 0; i< users.size();i++){
                        if (!users.get(i).equals(profile.getEmail()) && !users.get(i).contains(searchLabel)) {
                            list.add(v.getId());
                        }
                    }
                });
                controller.searchEnded(list);
                done = true;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza el ultimo mensaje visto
     */
    public void updateLastViewedMessage() {
        Channel currentChannel = getCurrentChannel();
        if (getCurrentChannel() != null) {
            if (currentChannel.getMessages() != null && !currentChannel.getMessages().isEmpty())
                currentChannel.setLastViewedMessageId(currentChannel.getMessages().get(currentChannel.getMessages().size()-1).getMessageId());
            controller.setSeenMessages(currentChannel.getId());
            LastView lastView = new LastView(profile.getEmail(), currentChannel.getLastViewedMessageId(), currentChannel.getId());
            serverCommunication.sendPacket(lastView);
        }
    }

    /**
     * Getter de todos los canales publicos
     * @return mapa de los canales publicos
     */
    public HashMap<Integer, Channel> getPublicChannels() {
        return publicChannels;
    }

    /**
     * Getter de todos los canales privados
     * @return mapa de los canales privados
     */
    public HashMap<Integer, Channel> getPrivateChannels() {
        return privateChannels;
    }

    /**
     * Retorna una lista de los perfiles en un canal
     *
     * @param channel canal del cual obtener la informacion
     * @return lista con perfiles
     */
    public ArrayList<Profile> getProfiles(Channel channel) {
        ArrayList<Profile> ret = new ArrayList<>();
        for (String e : channel.getUsers()) {
            ret.add(profiles.get(e));
        }
        return ret;
    }

    /**
     * Borra un canal del modelo
     *
     * @param idChannel titulo del canal
     * @return devuelve true si el canal actual es el que hay que borrar
     */
    public boolean deleteChannel(int idChannel){
        Channel channelAux = publicChannels.get(idChannel);
        if(channelAux != null){
            publicChannels.remove(idChannel);
        }else{
            channelAux = privateChannels.get(idChannel);
            if(channelAux != null){
                privateChannels.remove(idChannel);
            }
        }
        if(currentChannel != null) {
            return idChannel == currentChannel.getId();
        }else{
            return false;
        }
    }

    /**
     * Agrega un canal al modelo
     *
     * @param channel canal a agregar
     * @return devuelve el nombre del canal, en caso de ser privado el nombre del otro usuario
     */
    public String addChannel(Channel channel){
        String otherUser = null;
        if(!channel.isPrivate()){
            publicChannels.put(channel.getId(),channel);
        }else{
            ArrayList<String> users = channel.getUsers();
            for(int i = 0; i<users.size();i++){
                if(!users.get(i).equals(profile.getEmail())){
                    otherUser = profiles.get(users.get(i)).getUsername();
                }
            }
            privateChannels.put(channel.getId(),channel);
        }
        profiles.get(profile.getEmail()).getIdChannels().add(channel.getId());
        profile.getIdChannels().add(channel.getId());
        if(otherUser == null){
            return channel.getName();
        }else{
            return otherUser;
        }
    }

    /**
     * Agrega un usuario a un canal
     *
     * @param profile   perfil a agregar
     * @param idChannel id del canal donde agregarlo
     * @return
     */
    public boolean addUserToChannel(Profile profile, int idChannel){
        Channel channelAux = publicChannels.get(idChannel);
        if(channelAux != null) {
            channelAux.getUsers().add(profile.getEmail());

        }else {
            channelAux = privateChannels.get(idChannel);
            if(channelAux != null){
                channelAux.getUsers().add(profile.getEmail());
            }
        }
        if(currentChannel != null) {
            if (idChannel == currentChannel.getId()) {
                currentChannel = channelAux;
                return true;
            }
        }
        return false;
    }

    /**
     * Actualiza el estado de un perfil
     *
     * @param email del perfil al cual actualizar
     * @param status estado nuevo
     * @return el id del canal si ha encontrado el usuario donde esta, -1 si no
     */
    public int updateStatus(String email, String status){
        if(profiles != null) {
            Profile profileAux = profiles.get(email);
            if (profileAux != null) {
                profileAux.setStatus(status);
            }
        }
        for(Channel c : privateChannels.values()){
            if (c.getUsers().contains(email)) {
                return c.getId();
            }
        }
        return -1;
    }

    /**
     * Dice si existe un chat privado con un usuario
     *
     * @param email el otro usuario en cuestion
     * @return -1 si no existe, o el id del canal en caso de que si
     */
    public int existsPrivateChannel(String email){
        for(Channel c : privateChannels.values()){
            if (c.getUsers().contains(email)) {
                return c.getId();
            }
        }
        return -1;
    }

    /**
     * Cambia el status de cada usuario con el que hay un chat privado
     */
    public void searchConnexionUsers(){
        profiles.forEach((k, v) ->{
            int idChannel = existsPrivateChannel(v.getEmail());
            if(!v.getEmail().equals(profile.getEmail()) && idChannel != -1){
                controller.changeConnection(idChannel,v.getStatus());
            }
        });
    }

    /**
     * Getter de todos los usuarios
     * @return mapa con todos los usuarios que tenemos
     */
    public HashMap<String, Profile> getUsers(){
        return profiles;
    }

    /**
     * Getter del nombre del canal privado que se indica
     * @param channel canal privado
     * @return
     */
    public String getPrivateChannelName(Channel channel) {
        if (channel.isPrivate()) {
            for (String u : channel.getUsers()) {
                if (!getProfile().getEmail().equals(u)) {
                    return profiles.get(u).getUsername();
                }
            }
            return "Name not found";
        } else {
            return null;
        }
    }

    /**
     * Obtiene el Status de un usuario
     * @param username user a buscar
     * @return String Status
     */
    public String getStatus(String username){
        for(Profile p : profiles.values()){
            if(p.getUsername().equals(username)){
                return p.getStatus();
            }
        }
        return null;
    }

    /**
     * Inserta una lista de perfiles al modelo
     *
     * @param profilesA lista de perfiles
     */
    public void insertProfiles(List<Profile> profilesA) {
        for (Profile pr : profilesA) {
            if (!profiles.containsKey(pr.getEmail())) {
                profiles.put(pr.getEmail(), pr);
            }
        }
    }

    /**
     * Compara si el canal es privado y creado por nosotros
     * @param idChannel id del canal
     * @return true si es nuestro false sino
     */
    public boolean loadChannel(int idChannel){
        return privateChannels.get(idChannel).getUsers().get(1).equals(profile.getEmail());
    }

    public void setCurrentChannel(Channel currentChannel) {
        this.currentChannel = currentChannel;
    }
}
