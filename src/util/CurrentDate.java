package util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util para generar fechas
 */
public class CurrentDate extends Date {

    private final SimpleDateFormat formatter;
    private final SimpleDateFormat formatterInfo;

    /**
     * Constructor
     */
    public CurrentDate() {
        super(System.currentTimeMillis());
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatterInfo = new SimpleDateFormat("dd-MM-yyyy");
    }

    /**
     * Constructor
     * @param date fecha
     */
    public CurrentDate(long date) {
        super(date);
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatterInfo = new SimpleDateFormat("dd-MM-yyyy");
    }

    /**
     * Devuelve la fecha en un buen formato para la DB
     * @return fecha en string
     */
    @Override
    public String toString() {
        return formatter.format(this);
    }

    /**
     * Otro formato de fecha
     * @return fecha en string
     */
    public String infoToString() { return formatterInfo.format(this);}

    /**
     * Devuelve si cd2 es un dia distinto a cd1
     * @param oldDay dia antigui
     * @param newDay dia nuevo
     * @return si es nuevo dia o no
     */
    public static boolean nextDay(Date oldDay, Date newDay){

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("yyyy-MM-dd");

        return formatter.format(oldDay).compareTo(formatter.format(newDay)) < 0;
    }
}
