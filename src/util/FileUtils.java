package util;

import network.packet.File;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Clase auxiliar para utilidades con los archivos
 */
public class FileUtils {
    public static final String[] PICTURE_EXTENSIONS = {"png", "jpg", "jpeg", "bmp"};

    /**
     * Devuelve un BufferedImage del file
     * @param file
     * @return Devuelve un BufferedImage o null si no se puede cargar la imagen
     */
    public static BufferedImage getImageBufferedImage(File file){

        InputStream in = new ByteArrayInputStream(file.getData());
        BufferedImage image = null;
        try {
            image = ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    /**
     * Devuelve un placeholder de la imagen del file
     * @param file archivo a verificar
     * @return un placeholder o null si no se puede cargar la imagen
     */
    public static BufferedImage getFileBufferedImage(File file){

        String ext = file.getExt() ;
        BufferedImage image = null;
        try {
            switch (ext) {
                case "pdf":
                    image = ImageIO.read(new java.io.File("resources/pdf.png"));
                    break;
                case "txt":
                    image = ImageIO.read(new java.io.File("resources/txt.png"));
                    break;
                case "zip":
                    image = ImageIO.read(new java.io.File("resources/zip.png"));
                    break;
                default:
                    image = ImageIO.read(new java.io.File("resources/file_icon.png"));
                    break;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Verifica si el archivo es imagen o no a través de su nombre
     * @param fileName nombre del archivo a examinar
     * @return true si es imagen, false si no es imagen
     */
    public static boolean isPicture(String fileName) {
        String ext = getExtension(fileName);
        for (int i = 0; i < PICTURE_EXTENSIONS.length; i++) {
            if (PICTURE_EXTENSIONS[i].toLowerCase().equals(ext.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Devuelve la extension del nombre de un archivo
     * @param fileName
     * @return la extension del archivo
     */
    public static String getExtension(String fileName){
        String[] s = fileName.split("\\.");
        return s.length > 0 ? s[s.length-1]:"";
    }

    /**
     * baja una archivo al ordenador en caso de poderlo hacerlo
     * @param f archivo a descargar
     */
    public static void donwloadFile(File f, Component parent) {
        JFileChooser chooser = new JFileChooser();
        chooser.setSelectedFile(new java.io.File(f.getName()));
        int result = chooser.showSaveDialog(parent);

        if (result == JFileChooser.APPROVE_OPTION) {
            java.io.File file = new java.io.File(chooser.getSelectedFile().getAbsolutePath());
            FileOutputStream fos = null;

            try {
                fos = new FileOutputStream(file);
                fos.write(f.getData());
                fos.close();

                JOptionPane.showMessageDialog(parent, "Guardado exitosamente!", "", JOptionPane.INFORMATION_MESSAGE);

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("No se pudo descargar");
            }
        }
    }
}